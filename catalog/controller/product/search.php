<?php
class ControllerProductSearch extends Controller {
    public function index() {

        $this->load->language('product/category');
        $this->load->language('product/search');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $this->load->model('elasticsearch/products');

        $url = '';

        if (isset($this->request->get['search_method'])) {
            $search_method= $this->request->get['search_method'];
            $url .= '&search_method=' . $search_method;
        } else {
            $search_method= 'k';
        }

        if (isset($this->request->get['search'])) {
            $search = strtolower($this->model_elasticsearch_products->translate($this->request->get['search']));
            $search = htmlspecialchars_decode($search);
            $url .= '&search=' . urlencode($search);

        } else {
            $search = '';
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
            $url .= '&sort=' . $sort;
        } else {
            $sort = '';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
            $url .= '&order=' . $order;
        } else {
            $order = '';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = (int)$this->request->get['limit'];
            //$url .= '&limit=' . $limit;
        } else {
            $limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
        }

        if (isset($this->request->get['website'])) {
            $website = $this->request->get['website'];
            $url .= '&website=' . $website;
        } else {
            $website = '';
        }

        if (isset($this->request->get['brand'])) {
            $brand = $this->request->get['brand'];
            $brand = htmlspecialchars_decode($brand);
            $url .= '&brand=' . urlencode($brand);
        } else {
            $brand = '';
        }

        if (isset($this->request->get['from'])) {
            $price_start = $this->request->get['from'];
            $url .= '&from=' . $price_start;
        } else {
            $price_start = '';
        }

        if (isset($this->request->get['to'])) {
            $price_end = $this->request->get['to'];
            $url .= '&to=' . $price_end;
        } else {
            $price_end = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data=$this->setLanguage($data);
        $data['search']=$search;

        if (isset($this->request->get['path'])) {

            $path = '';

            $parts = explode('_', (string)$this->request->get['path']);

            $category_id = array_pop($parts);
            if(empty($parts)){
                $path=$category_id;
            }
            foreach ($parts as $path_id) {
                if (!$path) {
                    $path = $path_id;
                } else {
                    $path .= '_' . $path_id;
                }

                $category_info = $this->model_catalog_category->getCategory($path_id);

                if ($category_info) {
                    $data['breadcrumbs'][] = array(
                        'text' => $category_info['name'],
                        'href' => $this->url->link('product/search', 'path=' . $path . $url)
                    );
                }
            }
            $category_info = $this->model_catalog_category->getCategory($category_id);

            /*
            if (!isset($category_info['meta_title'])){
                $this->document->setTitle($category_info['meta_title']);
            }
            if (!isset($category_info['meta_description'])){
                $this->document->setDescription($category_info['meta_description']);
            }
            if (!isset($category_info['meta_keyword'])){
                $this->document->setKeywords($category_info['meta_keyword']);
            }
            if (!isset($category_info['name'])){
                $data['heading_title'] = $category_info['name'];
            }
            if (!isset($category_info['description'])){
                $data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
            }
            */

            // Set the last category breadcrumb
            //$url_part='search_method=c&path='.$path;
            $url=$url.'&path='.$path.'_'.$category_id;
            $data['breadcrumbs'][] = array(
                //'text' => $category_info['name'],
                //'href' => $this->url->link('product/search', $url)
            );

        } else {
            $category_id = "";//keyword search in all categories
            $path=0;
            $this->document->setTitle($search);
            $this->document->setDescription($search);
            $this->document->setKeywords($search);
            $data['heading_title'] = $search;
            //$url_part='search_method=k&search='.$search;
            $data['breadcrumbs'][] = array(
                'text' => $search,
                'href' => $this->url->link('product/search', $url)
            );
            if ($search=='') {
                $search='milk powder';
            }
        }
        $filter_data = array(
            'filter_name' => $search,
            'filter_category_id' => $category_id,
            //'filter_filter'      => $filter,
            'sort'               => $sort,
            'search_method'      => $search_method,
            'order'              => $order,
            'start'              => ($page - 1) * $limit,
            'limit'              => $limit,
            'website'            =>$website,
            'brand'              =>htmlspecialchars_decode($brand),
            'price_start'        =>$price_start,
            'price_end'          =>$price_end
        );

        $category_data=$this->loadCategories($category_id,$path,$url);
        $data=array_merge($data,$category_data);

        $products_data=$this->loadProducts($filter_data,$url);
        $product_total=$products_data['product_total'];
        $data=array_merge($data,$products_data);

        $sort_data=$this->setSorts($url);
        $data=array_merge($data,$sort_data);

        $limit_data=$this->setLimits($url);
        $data=array_merge($data,$limit_data);

        // http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
        $page_data=$this->loadPagination($product_total, $page, $limit, $url);
        $data=array_merge($data,$page_data);

        $data['remove_filter_url']= $this->url->link('product/search', $url, true);
        if (!empty($brand)) {
            $url .= '&brand=' . $brand;
        }

        $data['sort'] = $sort;
        $data['order'] = $order;
        $data['limit'] = $limit;
        $data['form_action'] = $this->url->link('product/search', $url, true);
        $data['continue'] = $this->url->link('common/home');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('product/category', $data));
    }

    private function setLanguage($data){

        $data['text_refine'] = $this->language->get('text_refine');
        $data['text_empty'] = $this->language->get('text_empty');
        $data['text_quantity'] = $this->language->get('text_quantity');
        $data['text_manufacturer'] = $this->language->get('text_manufacturer');
        $data['text_model'] = $this->language->get('text_model');
        $data['text_price'] = $this->language->get('text_price');
        $data['text_tax'] = $this->language->get('text_tax');
        $data['text_points'] = $this->language->get('text_points');
        $data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
        $data['text_sort'] = $this->language->get('text_sort');
        $data['text_limit'] = $this->language->get('text_limit');
        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['button_list'] = $this->language->get('button_list');
        $data['button_grid'] = $this->language->get('button_grid');
        $data['text_wishlist'] = $this->language->get('text_wishlist');
        $data['text_quickview'] = $this->language->get('text_quickview');
        $data['button_search'] = $this->language->get('button_search');
        $data['text_keyword'] = $this->language->get('text_keyword');
        $data['dropdown_more'] = $this->language->get('dropdown_more');
        $data['dropdown_less'] = $this->language->get('dropdown_less');
        
        return $data;
    }

    private function loadCategories($category_id,$path,$url){
        $data['categories'] = array();

        $results = $this->model_catalog_category->getCategories($category_id);

        foreach ($results as $result) {
            $filter_data = array(
                'filter_category_id'  => $result['category_id'],
                'filter_sub_category' => true
            );

            $data['categories'][] = array(
                'name' => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                'href' => $this->url->link('product/search', 'path=' . $path . '_' . $result['category_id'] . $url)
            );
        }
        return $data;
    }

    private function loadProducts($filter_data,$url){
        $data['related_categories']= array();
        $data['products'] = array();
        $data['brands'] = array();
        $data['categories']= array();
        $results = $this->model_elasticsearch_products->getProducts($filter_data);
        $data['product_total'] = $results['hits']['total'];
        foreach($results['aggregations']['goods_website']['buckets']as $website){
            $data['websites'][]= array(
              'website'=> $website['key'],
              'doc_count'=> $website['doc_count'],
              'href'=> $this->url->link('product/search', $url . '&website='.urlencode ($website['key']))
            );
        }
        foreach($results['aggregations']['goods_brand']['buckets']as $brand){
            $data['brands'][]= array(
                'brand'=> $brand['key'],
                'doc_count'=> $brand['doc_count'],
                'href'=> $this->url->link('product/search', $url . '&brand='.urlencode ($brand['key']))
            );
        }
        foreach ($results['aggregations']['goods_category_id']['buckets'] as $category){
            if($category ['key']!="0"){
                $category_name = $this->model_catalog_category->getCategoryMdata($category['key']);
                $data['categories'][]=array(
                    'category_id' => $category['key'],
                    'name'        => $category_name['category_name_cn'],
                    'goods_count' => $category ['doc_count'],
                    'href'        => $this->url->link('product/search', 'path='. $category['key'] . $url)
                );
            }
        }
        $data['price_range']=array(
            '0' => array('value'=> '0-200','href' => $this->url->link('product/search', $url . '&from=0&to=200')),
            '1' => array('value'=> '201-500','href' => $this->url->link('product/search', $url . '&from=201&to=500')),
            '2' => array('value'=> '501-1000','href' => $this->url->link('product/search', $url . '&from=501&to=1000')),
            '3' => array('value'=> '>1000','href' => $this->url->link('product/search', $url . '&from=1001&to=100000')),
        );

        foreach ($results['hits']['hits'] as $result) {
            $result['des'] = $this->model_elasticsearch_products->getProductDescription($result['_source']['goods_uuid']);

            $result['tax_class_id']=9;//9 is taxable goods,10 is download goods.

            if ((float)$result['_source']['goods_current_price']<(float)$result['_source']['goods_mrsp_price']) {
                $special = $this->currency->format($this->tax->calculate($result['_source']['goods_current_price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $special = false;
            }
            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($result['_source']['goods_current_price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price = false;
            }

            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float)$result['_source']['goods_current_price'] ? $result['_source']['goods_mrsp_price'] : $result['_source']['goods_current_price'], $this->session->data['currency']);
            } else {
                $tax = false;
            }
            if(empty($result['des'])){
                $result['des'][0]['_source']['goods_description_history']="";
            }
            if($result['_source']['goods_main_image_url_local']!=''){
                $image=$result['_source']['goods_main_image_url_local'];
            }else{
                $image=$result['_source']['goods_main_image_url'];
            }
            $data['products'][] = array(
                'product_id'  => $result['_source']['goods_uuid'],
                'thumb'       => $image,
                'name'        => $result['_source']['goods_title'],
                'name_cn'        => $result['_source']['goods_title_cn'],
                'description' => utf8_substr(strip_tags(html_entity_decode($result['des'][0]['_source']['goods_description_history'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                'price'       => $price,
                'special'     => $special,
                'tax'         => $tax,
                //'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
                'rating'      => $result['_source']['goods_review_current_stars'],
                'href'        => $this->url->link('product/product', 'product_id=' . $result['_source']['goods_uuid'])
            );
        }
        return $data;
    }
    private function setSorts($url){
        $data['sorts'] = array();

        $data['sorts'][] = array(
            'text'  => $this->language->get('text_default'),
            'value' => '_score-desc',
            'href'  => $this->url->link('product/search', $url . '&sort=_score&order=desc')
        );

        /*		$data['sorts'][] = array(
                    'text'  => $this->language->get('text_name_asc'),
                    'value' => 'goods_title-asc',
                    'href'  => $this->url->link('product/search', $url_part . '&sort=goods_title&order=asc' . $url)
                );

                $data['sorts'][] = array(
                    'text'  => $this->language->get('text_name_desc'),
                    'value' => 'goods_title-desc',
                    'href'  => $this->url->link('product/search', $url_part . '&sort=goods_title&order=desc' . $url)
                );*/

        $data['sorts'][] = array(
            'text'  => $this->language->get('text_price_asc'),
            'value' => 'goods_current_price-asc',
            'href'  => $this->url->link('product/search', $url . '&sort=goods_current_price&order=asc')
        );

        $data['sorts'][] = array(
            'text'  => $this->language->get('text_price_desc'),
            'value' => 'goods_current_price-desc',
            'href'  => $this->url->link('product/search', $url . '&sort=goods_current_price&order=desc')
        );

        if ($this->config->get('config_review_status')) {
            $data['sorts'][] = array(
                'text'  => $this->language->get('text_rating_desc'),
                'value' => 'goods_review_current_stars-desc',
                'href'  => $this->url->link('product/search', $url . '&sort=goods_review_current_stars&order=desc')
            );

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_rating_asc'),
                'value' => 'goods_review_current_stars-asc',
                'href'  => $this->url->link('product/search', $url . '&sort=goods_review_current_stars&order=asc')
            );
        }

        /*		$data['sorts'][] = array(
                    'text'  => $this->language->get('text_model_asc'),
                    'value' => 'goods_model-asc',
                    'href'  => $this->url->link('product/search', $url_part . '&sort=goods_model&order=asc' . $url)
                );

                $data['sorts'][] = array(
                    'text'  => $this->language->get('text_model_desc'),
                    'value' => 'goods_model-desc',
                    'href'  => $this->url->link('product/search', $url_part . '&sort=goods_model&order=desc' . $url)
                );*/
        return $data;
    }

    private function setLimits($url){
        $data['limits'] = array();

        $limits = array_unique(array($this->config->get($this->config->get('config_theme') . '_product_limit'), 30, 60, 75, 90));

        sort($limits);

        foreach($limits as $value) {
            $data['limits'][] = array(
                'text'  => $value,
                'value' => $value,
                'href'  => $this->url->link('product/search', $url . '&limit=' . $value)
            );
        }
        return $data;
    }

    private function loadPagination($product_total,$page,$limit,$url){
        $pagination = new Pagination();
        $pagination->total = $product_total;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->url = $this->url->link('product/search', $url . '&page={page}');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

        if ($page == 1) {
            $this->document->addLink($this->url->link('product/search', $url, true), 'canonical');
        } elseif ($page == 2) {
            $this->document->addLink($this->url->link('product/search', $url, true), 'prev');
        } else {
            $this->document->addLink($this->url->link('product/search', $url . '&page='. ($page - 1), true), 'prev');
        }

        if ($limit && ceil($product_total / $limit) > $page) {
            $this->document->addLink($this->url->link('product/search', $url . '&page='. ($page + 1), true), 'next');

        }
        return $data;
    }
}