<?php

/* ---------------------------------------------------------------------------------- */
/*  OpenCart BasicOverride template adaptor (used by the Override Engine)             */
/*                                                                                    */
/*  Copyright © 2016 by J.Neuhoff (www.mhccorp.com)                                   */
/*                                                                                    */
/*  This file is part of the Override Engine for OpenCart.                            */
/*                                                                                    */
/*  OpenCart is free software: you can redistribute it and/or modify                  */
/*  it under the terms of the GNU General Public License as published by              */
/*  the Free Software Foundation, either version 3 of the License, or                 */
/*  (at your option) any later version.                                               */
/*                                                                                    */
/*  OpenCart is distributed in the hope that it will be useful,                       */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of                    */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                     */
/*  GNU General Public License for more details.                                      */
/*                                                                                    */
/*  You should have received a copy of the GNU General Public License                 */
/*  along with OpenCart.  If not, see <http://www.gnu.org/licenses/>.                 */
/* ---------------------------------------------------------------------------------- */

namespace Template;
final class BasicOverride {
	private $data = array();
	
	public function set($key, $value) {
		$this->data[$key] = $value;
	}

	protected function isAdmin() {
		return defined('DIR_CATALOG');
	}

	public function render($template) {
		$file = '';
		if ($this->isAdmin()) {
			if (is_file(DIR_CACHE.'override/admin_view_template_'.str_replace('/','_',$template))) {
				$file = DIR_CACHE.'override/admin_view_template_'.str_replace('/','_',$template);
			} else if (is_file(DIR_TEMPLATE.$template)) {
				$file = DIR_TEMPLATE.$template;
			}
		} else {
			if (is_file(DIR_CACHE.'override/catalog_view_theme_'.str_replace('/','_',$template))) {
				$file = DIR_CACHE.'override/catalog_view_theme_'.str_replace('/','_',$template);
			} else if (is_file(DIR_TEMPLATE.$template)) {
				$file = DIR_TEMPLATE.$template;
			}
		}

		if ($file) {
			extract($this->data);

			ob_start();

			require($file);

			$output = ob_get_contents();

			ob_end_clean();

			return $output;
		} else {
			trigger_error("Error: Could not load template for '" . $template . "'!");
			exit();
		}
	}	
}