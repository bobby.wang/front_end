<?php
/**
 * Created by PhpStorm.
 * User: Bill
 * Date: 2016-07-14
 * Time: 11:36 PM
 */
require (DIR_VENDOR.'autoload.php');
use Elasticsearch\ClientBuilder;
class ModelElasticsearchAdminProducts extends Model{

    private $hosts;
    private $config;
    private $elasticsearch;

    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->hosts = unserialize(ES_HOST);
        $this->config = unserialize(ES_INDEX);
        $this->elasticsearch = ClientBuilder::create()->setHosts($this->hosts)->build();
        $this->google_key = 'AIzaSyAeSQFrACMCWAZ0YubXlXc5Wrdj6Oiwb2g';
        $this->translateURL = 'https://www.googleapis.com/language/translate/v2?key='.$this->google_key.'&target=en';
        $this->detectURL = 'https://www.googleapis.com/language/translate/v2/detect?key=AIzaSyAeSQFrACMCWAZ0YubXlXc5Wrdj6Oiwb2g';
    }
    
    public function translate($keyword){
        $result = json_decode($this->do_curl_get_request($this->translateURL,$keyword,30));
        if ($result!=''){
            $keyword_translated = $result->data->translations[0]->translatedText;
            return $keyword_translated;
        }else{
            return "";
        }
//        $data= json_encode($array);
//        $price = $this->do_curl_get_request("http://api.bigdataapi.com:20008/retriver/price",$data,30);//请求成功将会返回价格
    }


    private function do_curl_get_request($url, $data, $timeout = 30){
        if ($url == '' || $timeout <= 0) {
            return false;
        }
        if ($data != null) {
//            $url = $url . '&q=' . http_build_query($data);
             $url = $url . '&q=' . urlencode($data);
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //将curl_exec()获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_HEADER, false); //启用时会将头文件的信息作为数据流输出
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout); //设置cURL允许执行的最长秒数
        $output = curl_exec($ch);
        if ($output === FALSE) {
            #echo "cURL Error: " . curl_error($ch);
            return false;
        }
        return $output;
    }

    public function getProducts($condition)
    {
        $params = array();
        //$keyword_cn= '';
        $keyword_en = '';

        if (!empty($condition['filter_name'])) {
            $keyword_en = $condition['filter_name'];
            //$keyword_en=$this->translate($keyword);
        }
        $params['index'] = $this->config['goods'][0];
        $query = [];
        $filter = [];
        //关键词
        if (isset($keyword_en) && strlen($keyword_en) > 0) {
            $keyword_en_arr = preg_split('/\s+/', $keyword_en);
            if (count($keyword_en_arr) < 7) {
                foreach ($keyword_en_arr as $k => $v) {
                    $query['must'][] = [
                        'match' => [
                            'goods_search' => $v
                        ]
                    ];
                }
            } else {
                foreach ($keyword_en_arr as $k => $v) {
                    $query['must'][] = [
                        'query_string' => [
                            'default_field' => 'goods_search',
                            'query' => $v
                        ]
                    ];
                }
            }
            $filter['must'][] = [
                'term' => [
                    'goods_status' => 1
                ]
            ];
            $filter['must_not'][] = [
                'term' => [
                    'goods_category_id' => 9
                ]
            ];
        }

        //主类目
       if (isset($condition['filter_category_id'])&& intval($condition['filter_category_id'])!=9) {
           if (is_array($condition['filter_category_id'])) {
               $filter['must'][] = [
                   'terms' => [
                       'goods_category_id' => $condition['filter_category_id']
                   ]
               ];
           } else {
               if (intval($condition['filter_category_id']) > 0) {

                   $filter['must'][] = [
                       'prefix' => [
                           'goods_category_id' => $condition['filter_category_id']
                       ]
                   ];
               }
           }
           $filter['must'][] = [
               'term' => [
                   'goods_status' => 1
               ]
           ];
       }

        //起始价格
        if(!empty($condition['price_start'])){
            $filter['must'][]=[
                'range'=>[
                    'shop_price'=>[
                        'gte'=>$condition['price_start']
                    ]
                ]
            ];
        }
        //最大价格
        if(isset($condition['price_end'])&&$condition['price_end']>0){
            $filter['must'][]=[
                'range'=>[
                    'shop_price'=>[
                        'lte'=>$condition['price_end']
                    ]
                ]
            ];
        }
        //品牌
        if(!empty($condition['brand'])){
            $filter['must'][]=[
                'term'=>[
                    'goods_brand'=>$condition['brand']
                ]
            ];
        }
        //采集站点
        if(!empty($condition['website'])){
            $filter['must'][]=[
                'term'=>[
                    'goods_website'=>$condition['website']
                ]
            ];
        }


        if (!empty($query)) {
            $params['body']['query']['filtered']['query']['bool'] = $query;
        }
        if (!empty($filter)) {
            $params['body']['query']['filtered']['filter']['bool'] = $filter;
        }

        if ($condition['search_method']=='k'&& $condition['sort']==''){
            $params['body']['sort'] = [
                '_score' => [
                    'order' => 'desc'
                ],
                'goods_order_score' =>[
                    'order' => 'desc'
                ]
            ];
        }
        else{
            $params['body']['sort'] = [
                'goods_order_score' =>[
                    'order' => 'desc'
                ]
            ];
        }
        //商品排序
        if (!empty($condition['order'])) {
            $params['body']['sort'] = [
                $condition['sort'] => [
                    'order' => $condition['order']
                ]
            ];
        }

        //搜索分页
        //$this->size = array_key_exists('size', $condition) ? $condition['size'] : $this->size;
        $params['body']['size'] = $condition['limit'];
        //$page = array_key_exists('page', $condition) ? $condition['page'] : 0;
        $params['body']['from'] = $condition['start'];
        //对采集站点和品牌进行分组统计
        $params['body']['aggs'] = [
            'goods_website' => [
                'terms' => [
                    'field' => 'goods_website',
                    'size' => 10,
                    'order' => [
                        "_count" => "desc"
                    ]
                ]
            ],
            'goods_brand' => [
                'terms' => [
                    'field' => 'goods_brand',
                    'size' => 10,
                    'order' => [
                        "_count" => "desc"
                    ]
                ]
            ],
            'goods_category_id' => [
                'terms' => [
                    'field' => 'goods_category_id',
                    'size' => 10,
                    'order' => [
                        "_count" => "desc"
                    ]
                ]
            ]
        ];

        //Topic Json 字符串
        if (!empty($condition['json'][0])) {
            $topic_json = $condition['json'][0]['condition'];
            $params['body'] = $topic_json;
        }

        $final_json = json_encode($params);
        //echo $final_json;

        //开始在elasticsearch中进行搜索
        $results = $this->elasticsearch->search($params);

        return $results;
     }

    public function getProductDetail($goods_uuid){

        $params['index'] = $this->config['goods'][0];

        if (isset($goods_uuid)) {

            $query['must'][] = [
                'term' => [
                    'goods_uuid' => $goods_uuid
                ]
            ];
        }

        if (!empty($query)) {
            $params['body']['query']['bool'] = $query;
        }

        $results = $this->elasticsearch->search($params);
        return $results['hits']['hits'][0]['_source'];
    }

    public function getProductDescription($goods_uuid){

        $params['index'] = $this->config['description'][0];

        if (isset($goods_uuid)) {

                $query['must'][] = [
                    'term' => [
                        'goods_uuid' => $goods_uuid
                    ]
                ];
        }

        if (!empty($query)) {
            $params['body']['query']['bool'] = $query;
        }

        $results = $this->elasticsearch->search($params);
        return $results['hits']['hits'];
    }

    public function getProductImages($goods_uuid){

        $params['index'] = $this->config['images'][0];

        if (isset($goods_uuid)) {

            $query['must'][] = [
                'term' => [
                    'goods_uuid' => $goods_uuid
                ]
            ];
        }

        if (!empty($query)) {
            $params['body']['query']['bool'] = $query;
        }

        $results = $this->elasticsearch->search($params);
        return $results['hits']['hits'];
    }

    public function getProductPrice($goods_uuid){

        $params['index'] = $this->config['price'][0];

        if (isset($goods_uuid)) {

            $query['must'][] = [
                'term' => [
                    'goods_uuid' => $goods_uuid
                ]
            ];
        }

        if (!empty($query)) {
            $params['body']['query']['bool'] = $query;
        }

        $results = $this->elasticsearch->search($params);
        return $results['hits']['hits'];
    }

    public function getProductVariation($goods_asin,$goods_uuid){

        $params['index'] = $this->config['goods'][0];

        if (isset($goods_asin)) {

            $query['must'][] = [
                'term' => [
                    'goods_asin' => $goods_asin
                ]
            ];

            $query['must_not'][] = [
                'term' => [
                    'goods_uuid' => $goods_uuid
                ]
            ];
        }

        if (!empty($query)) {
            $params['body']['query']['bool'] = $query;
        }

        $results = $this->elasticsearch->search($params);
        return $results['hits']['hits'];
    }

    public function getSuggestedGoods($filter_data){

        $params['index'] = $this->config['goods'][0];

        if (isset($filter_data['keyword']) && strlen($filter_data['keyword']) > 0) {
            $keyword_en_arr = preg_split('/\s+/', $filter_data['keyword']);
            if (count($keyword_en_arr) < 7) {
                foreach ($keyword_en_arr as $k => $v) {
                    $query['must'][] = [
                        'match' => [
                            'goods_search' => $v
                        ]
                    ];
                }
            } else {
                foreach ($keyword_en_arr as $k => $v) {
                    $query['must'][] = [
                        'query_string' => [
                            'default_field' => 'goods_search',
                            'query' => $v
                        ]
                    ];
                }
            }
            $filter['must'][] = [
                'term' => [
                    'goods_status' => 1
                ]
            ];
            $filter['must_not'][] = [
                'term' => [
                    'goods_category_id' => 9
                ]
            ];
        }

        if (isset($filter_data['category'])&& intval($filter_data['category'])!=9) {

                $filter['must'][] = [
                    'prefix' => [
                        'goods_category_id' => $filter_data['category']
                    ]
                ];
                $filter['must'][] = [
                    'term' => [
                        'goods_status' => 1
                    ]
                ];
        }

        $params['body']['sort'] = [
            $filter_data['sort'] => [
                'order' => $filter_data['order']
            ]
        ];

        $params['body']['size'] = $filter_data['limit'];

        if (!empty($query)) {
            $params['body']['query']['filtered']['query']['bool'] = $query;
        }
        if (!empty($filter)) {
            $params['body']['query']['filtered']['filter']['bool'] = $filter;
        }

        $final_json = json_encode($params);

        $results = $this->elasticsearch->search($params);
        return $results['hits']['hits'];
    }

}
