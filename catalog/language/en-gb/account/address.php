<?php
// Heading 
$_['heading_title']        = '地址簿';

// Text
$_['text_account']         = '我的帐号';
$_['text_address_book']    = '地址簿';
$_['text_edit_address']    = '编辑地址';
$_['text_add']             = '地址已新增 !';
$_['text_edit']            = '地址已更新 !';
$_['text_delete']          = '地址已删除 !';
$_['text_empty']           = '您的帐号中没有地址资料 !';

// Entry
$_['entry_firstname']      = '收货人姓名';
$_['entry_lastname']       = '姓氏';
$_['entry_shenfenzheng']   = '收货人身份证号';
$_['entry_company']        = '公司';
$_['entry_address_1']      = '地址 ';
$_['entry_address_2']      = '地址 (2)';
$_['entry_postcode']       = '邮政编码';
$_['entry_phone_number']       = '电话号码';
$_['entry_city']           = '市县区';
$_['entry_country']        = '所在国家';
$_['entry_zone']           = '省市/地区';
$_['entry_default']        = '默认收货地址';

// Error
$_['error_delete']         = '警告： 您必须至少有一组地址！';
$_['error_default']        = '警告： 您不能删除默认收货地址！';
$_['error_firstname']      = '名字必须在 1 到 32 字之间！';
$_['error_lastname']       = '姓氏必须在 1 到 32 字之间！';
$_['error_shenfenzheng']   = '身份证不符合格式！';
$_['error_vat']            = '无效的发票增值码！';
$_['error_address_1']      = '地址必须在 3 到 128 字之间！';
$_['error_postcode']       = '邮递区号必须是 6位数字！';
$_['error_city']           = '市县区必须在 2 到 128 字之间！';
$_['error_country']        = '请选择一个国家！';
$_['error_zone']           = '请选择一个省市/地区！';
$_['error_custom_field']   = '%s 必须填写！';
$_['error_custom_field_validate'] = '%s 无效!';
