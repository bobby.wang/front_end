<?php echo $header; ?>


<div>

<div id="content">

  <?php echo $column_left;?>

   <div id="magik-slideshow" class="magik-slideshow">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
            <?php echo $slider_left;?>
        </div>   <!--header search bar for mobile version-->

        <div class = "col-sm-12 col-xs-12 visible-xs">
           <div class="mobile-menu" >
           <div class="mm-search-header">
               <div id="mm_search-home">
                    <div class="input-group-header">
                      <div class="input-group-btn">
                            <button id="mm-submit-button-search-header" class="btn btn-default">
                            <i class="fa fa-search"></i>
                            </button>
                      </div>
                       <input id="srch-term" class="form-control simple" type="text" name="search_mm" maxlength="70" value="<?php echo $search;?>" placeholder="<?php echo $text_search; ?>">
                    </div>
               </div>
           </div>
           </div>
        </div>
        <div class="col-sm-12 col-xs-12 visible-xs">
            <ul class="mobile-cate">
                <li><a href="index.php?route=product/search&path=03"><img src="catalog/view/theme/boltdemo1/image/fyry.png" alt=""></a><br>&nbsp;&nbsp;妇婴日用</li>
                <li><a href="index.php?route=product/search&path=02"><img src="catalog/view/theme/boltdemo1/image/mzgh.png" alt=""></a><br>&nbsp;&nbsp;美妆个护</li>
                <li><a href="index.php?route=product/search&path=04"><img src="catalog/view/theme/boltdemo1/image/qdsc.png" alt=""></a><br>&nbsp;&nbsp;潮流轻奢</li>
                <li><a href="index.php?route=product/search&path=01"><img src="catalog/view/theme/boltdemo1/image/spbj.png" alt=""></a><br>&nbsp;&nbsp;食品保健</li>
            </ul>
        </div>
        <!--<div class="col-md-3">
            <?php echo $slider_right;?>
        </div>-->
    </div>
    </div>
  </div>
<div class="index-content-page">
<div class="container">
  <div class="row">
  <?php echo $top_right;?>
  </div>
</div>
</div>
  <?php echo $content_bottom; ?>
</div>
</div>
<?php echo $footer; ?>

<!--<script type="text/javascript">
$.each($(".product-image img"),function(){
        var self = $(this);
        var FitWidth = 280;
        var FitHeight = 340;
        var image = new Image();
        image.onload = function(){
            if (image.width > 0 && image.height > 0) {
                if (image.width / image.height >= FitWidth / FitHeight) {
                    if (image.width > FitWidth) {
                        self[0].width = FitWidth;
                        self[0].height = (image.height * FitWidth) / image.width 
                    } else {
                        self[0].width = image.width;
                        self[0].height = image.height
                    }
                } else {
                    if (image.height > FitHeight) {
                        self[0].height = FitHeight;
                        self[0].width = (image.width * FitHeight) / image.height
                    } else {
                        self[0].width = image.width;
                        self[0].height = image.height
                    }
                }
            }
        }
        image.src = self.attr('src');
 });
</script>-->