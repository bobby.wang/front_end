ALTER TABLE `oc_address` 
CHANGE COLUMN `lastname` `lastname` VARCHAR(32) NULL ,
CHANGE COLUMN `address_2` `address_2` VARCHAR(128) NULL ,
ADD COLUMN `zone_name` VARCHAR(45) NULL AFTER `zone_id`,
ADD COLUMN `shenfenzheng` VARCHAR(45) NULL AFTER `custom_field`,
ADD COLUMN `shenfenzheng_img1` VARCHAR(45) NULL AFTER `shenfenzheng`,
ADD COLUMN `shenfenzheng_img2` VARCHAR(45) NULL AFTER `shenfenzheng_img1`;

ALTER TABLE `oc_customer` 
CHANGE COLUMN `lastname` `lastname` VARCHAR(32) NULL ;

ALTER TABLE `oc_address`
ADD COLUMN `phone_number` VARCHAR(45) NULL AFTER `shenfenzheng_img2`;

CREATE TABLE `freelife_mdata_goods` (
  `seller_product_uuid` varchar(40) NOT NULL,
  `product_uuid` varchar(40) NOT NULL,
  `website_seller_uuid` varchar(40) NOT NULL,
  `seller_product_category` varchar(45) DEFAULT NULL,
  `seller_product_name` varchar(145) DEFAULT NULL,
  `seller_product_brand` varchar(50) DEFAULT NULL,
  `seller_product_model` varchar(50) DEFAULT NULL,
  `seller_product_variation` varchar(50) DEFAULT NULL,
  `seller_product_used` varchar(50) DEFAULT NULL,
  `seller_product_title` varchar(500) DEFAULT NULL,
  `seller_product_title_cn` varchar(500) DEFAULT NULL,
  `seller_product_current_price` decimal(10,2) DEFAULT NULL,
  `seller_product_current_original_price` decimal(10,2) DEFAULT NULL,
  `seller_product_original_msrp` decimal(10,2) DEFAULT NULL,
  `seller_product_original_currency` varchar(40) DEFAULT NULL,
  `seller_product_current_url` varchar(1000) DEFAULT NULL,
  `seller_product_current_main_image_url` varchar(1000) DEFAULT NULL,
  `seller_product_review_current_count` int(11) DEFAULT NULL,
  `seller_product_review_current_stars` decimal(5,4) DEFAULT NULL,
  `seller_product_order_score` int(5) DEFAULT NULL,
  `seller_product_quantity` int(5) DEFAULT NULL,
  `seller_product_status` tinyint(1) DEFAULT NULL,
  `product_vpn` varchar(45) DEFAULT NULL,
  `product_upc` varchar(45) DEFAULT NULL,
  `product_asin` varchar(45) DEFAULT NULL,
  `website_name` varchar(200) DEFAULT NULL,
  `seller_name` varchar(200) DEFAULT NULL,
  `is_manual` tinyint(1) DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`seller_product_uuid`),
  KEY `category` (`seller_product_category`),
  KEY `socore` (`seller_product_order_score`),
  KEY `prod_name` (`seller_product_name`),
  KEY `prod_brand` (`seller_product_brand`),
  KEY `update_time` (`update_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
