<?php global $config; ?>
<div class="side-nav-categories hidden-xs">
  <div class="block-title"><?php echo $heading_title; ?></div>
  <div class="box-content box-category">
    <ul>
      <?php $cnt=1;$cat_cnt=count($categories); foreach ($categories as $category) { ?>
      <li class="<?php if($cnt==$cat_cnt) echo "last";?>">
        <?php if ($category['category_id'] == $category_id) { ?>
       <a href="<?php echo $category['href']; ?>" class="active"> <i class="fa fa-<?php echo $config->get('magikfluence_topcaticon'.$category['category_id']); ?>"></i><?php echo $category['name']; ?>(<?=$category['goods_count']?>)</a>
        <?php } else { ?>
       <a href="<?php echo $category['href']; ?>"> <i class="fa fa-<?php echo $config->get('magikfluence_topcaticon'.$category['category_id']); ?>"></i><?php echo $category['name']; ?>(<?=$category['goods_count']?>)</a>
        <?php } ?>
      </li>
      <?php $cnt++;} ?>
    </ul>
  </div>
</div>