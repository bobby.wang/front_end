-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: localhost    Database: freelife_new
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `oc_customer_activity`
--

DROP TABLE IF EXISTS `oc_customer_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oc_customer_activity` (
  `customer_activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `url` varchar(500) DEFAULT NULL,
  `session_id` varchar(100) DEFAULT NULL,
  `customer_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`customer_activity_id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_customer_activity`
--

LOCK TABLES `oc_customer_activity` WRITE;
/*!40000 ALTER TABLE `oc_customer_activity` DISABLE KEYS */;
INSERT INTO `oc_customer_activity` VALUES (1,0,'url',NULL,'127.0.0.1','2016-09-18 10:40:37','http://local.freelife.com/index.php?route=product/search&path=04_0401_040101','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','0'),(2,0,'url',NULL,'127.0.0.1','2016-09-18 10:40:41','http://local.freelife.com/index.php?route=product/search&path=03_0302_030201','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','0'),(3,0,'url',NULL,'127.0.0.1','2016-09-18 10:40:59','http://local.freelife.com/index.php?route=checkout/cart','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','0'),(4,0,'url',NULL,'127.0.0.1','2016-09-18 10:41:03','http://local.freelife.com/index.php?route=account/login','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','0'),(5,2,'login','{\"customer_id\":\"2\",\"name\":\"Bob Feng\"}','127.0.0.1','2016-09-18 10:41:06',NULL,NULL,NULL),(6,2,'url',NULL,'127.0.0.1','2016-09-18 10:41:06','http://local.freelife.com/index.php?route=account/account','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','2'),(7,0,'url',NULL,'127.0.0.1','2016-09-18 10:45:13','http://local.freelife.com/index.php?route=account/logout','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','0'),(8,0,'url',NULL,'127.0.0.1','2016-09-18 10:45:15','http://local.freelife.com/index.php?route=common/home','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','0'),(9,0,'url',NULL,'127.0.0.1','2016-09-18 10:45:16','http://local.freelife.com/index.php?route=common/home','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','0'),(10,0,'url',NULL,'127.0.0.1','2016-09-18 10:45:20','http://local.freelife.com/index.php?route=account/login','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','0'),(11,2,'url',NULL,'127.0.0.1','2016-09-18 10:45:38','http://local.freelife.com/index.php?route=account/account','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','2'),(12,0,'url',NULL,'127.0.0.1','2016-09-18 10:45:53','http://local.freelife.com/index.php?route=account/logout','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','0'),(13,0,'url',NULL,'127.0.0.1','2016-09-18 10:45:56','http://local.freelife.com/index.php?route=common/home','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','0'),(14,0,'url',NULL,'127.0.0.1','2016-09-18 10:45:57','http://local.freelife.com/index.php?route=common/home','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','0'),(15,0,'url',NULL,'127.0.0.1','2016-09-18 10:46:00','http://local.freelife.com/index.php?route=account/login','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','0'),(16,2,'login','{\"customer_id\":\"2\",\"name\":\"Bob Feng\"}','127.0.0.1','2016-09-18 10:47:07',NULL,'U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o',NULL),(17,2,'url',NULL,'127.0.0.1','2016-09-18 10:47:35','http://local.freelife.com/index.php?route=account/account','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','2'),(18,0,'url',NULL,'127.0.0.1','2016-09-18 10:47:37','http://local.freelife.com/index.php?route=account/logout','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','0'),(19,0,'url',NULL,'127.0.0.1','2016-09-18 10:47:40','http://local.freelife.com/index.php?route=account/login','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','0'),(20,2,'login','{\"customer_id\":\"2\",\"name\":\"Bob Feng\"}','127.0.0.1','2016-09-18 10:47:49',NULL,'U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o',NULL),(21,2,'url',NULL,'127.0.0.1','2016-09-18 10:47:53','http://local.freelife.com/index.php?route=account/account','U9M7BTXKFuIoLnSeLNXWXTkk12bUCb6o','2');
/*!40000 ALTER TABLE `oc_customer_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'freelife_new'
--

--
-- Dumping routines for database 'freelife_new'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-18 10:49:15
