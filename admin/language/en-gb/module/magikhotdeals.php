<?php
// Heading
$_['heading_title']    = '<b style="color:red">Magik Hot Deals</b>';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified module Magik Hot Deals!';
$_['text_edit']        = 'Edit Specials Module';


// Entry
$_['entry_name']       = 'Module Name';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';
$_['entry_product_name']        = 'Select Product';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Magik Hot Deals module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';