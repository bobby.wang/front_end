<?php
class magikblog_ControllerCommonMenu extends ControllerCommonMenu {

    /* overridden method, this newly introduced function is always called
       before the final rendering
    */
    public function preRender( $template_buffer, $template_name, &$data ) {
        if ($template_name != 'common/menu.tpl') {
            return parent::preRender( $template_buffer, $template_name, $data );
        }        
       
       
          if($this->config->get('magikblog_status')==1) {    
        
            $this->load->language('magikblog/blog');
            $data['text_blog'] = $this->language->get('text_blog');
            $data['text_blog_category'] = $this->language->get('text_blog_category');
            $data['text_blog_post'] = $this->language->get('text_blog_post');
            $data['text_blog_comment'] = $this->language->get('text_blog_comment');
            $data['mgkblog_category'] = $this->url->link('magikblog/category', 'token=' . $this->session->data['token'], 'SSL');
            $data['mgkblog_post'] = $this->url->link('magikblog/article', 'token=' . $this->session->data['token'], 'SSL');
            $data['mgkblog_comment'] = $this->url->link('magikblog/comment', 'token=' . $this->session->data['token'], 'SSL');

            $this->load->helper( 'modifier' );
            $search = '<li><a href="<?php echo $report_affiliate_activity; ?>"><?php echo $text_report_affiliate_activity; ?></a></li>';
            $offset = 4;
            $index = 1;
            $add = <<<'EOT'
                    <li id="blog"><a class="parent"><i class="fa fa-rss"></i> <span><?php echo $text_blog; ?></span></a>
                    <ul>
                      <li><a href="<?php echo $mgkblog_category; ?>"><?php echo $text_blog_category; ?></a></li>
                      <li><a href="<?php echo $mgkblog_post; ?>"><?php echo $text_blog_post; ?></a></li>
                      <li><a href="<?php echo $mgkblog_comment; ?>"><?php echo $text_blog_comment; ?></a></li>
                    </ul>
                  </li>
EOT;
            $template_buffer = Modifier::modifyStringBuffer( $template_buffer, $search, $add, 'after', $offset, $index );
            
        }    
        // call parent method
        return parent::preRender( $template_buffer, $template_name, $data );
    }
}
?> 
