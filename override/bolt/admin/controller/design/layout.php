<?php
class bolt_ControllerDesignLayout extends ControllerDesignLayout {

    /* overridden method, this newly introduced function is always called
       before the final rendering
    */
public function preRender( $template_buffer, $template_name, &$data ) {
        
        if ($template_name != 'design/layout_form.tpl') {
            return parent::preRender( $template_buffer, $template_name, $data );
        }
        $this->load->language('design/layout');
        
        $data['text_slider_left'] = $this->language->get('text_slider_left');
        $data['text_slider_right'] = $this->language->get('text_slider_right');
        $data['text_top_left'] = $this->language->get('text_top_left');
        $data['text_top_right'] = $this->language->get('text_top_right');
        
        $this->load->helper( 'modifier' );
        $search = '<option value="column_right"><?php echo $text_column_right; ?></option>';
        $offset = 1;
        $index = 1;
        $add = <<<'EOT'
                     <?php if ($layout_module['position'] == 'slider_left') { ?>
                    <option value="slider_left" selected="selected"><?php echo $text_slider_left; ?></option>
                    <?php } else { ?>
                    <option value="slider_left"><?php echo $text_slider_left; ?></option>
                    <?php } ?>
                    <?php if ($layout_module['position'] == 'slider_right') { ?>
                    <option value="slider_right" selected="selected"><?php echo $text_slider_right; ?></option>
                    <?php } else { ?>
                    <option value="slider_right"><?php echo $text_slider_right; ?></option>
                    <?php } ?>
                    <?php if ($layout_module['position'] == 'top_left') { ?>
                    <option value="top_left" selected="selected"><?php echo $text_top_left; ?></option>
                    <?php } else { ?>
                    <option value="top_left"><?php echo $text_top_left; ?></option>
                    <?php } ?>
                    <?php if ($layout_module['position'] == 'top_right') { ?>
                    <option value="top_right" selected="selected"><?php echo $text_top_right; ?></option>
                    <?php } else { ?>
                    <option value="top_right"><?php echo $text_top_right; ?></option>
                    <?php } ?>
EOT;
      $template_buffer = Modifier::modifyStringBuffer( $template_buffer, $search, $add, 'after', $offset, $index );

      $search = '<option value="column_right"><?php echo $text_column_right; ?></option>';
      $offset = 0;
      $index = 2;
      $add = <<<'EOT'
      html += '    <option value="slider_left"><?php echo $text_slider_left; ?></option>';
      html += '    <option value="slider_right"><?php echo $text_slider_right; ?></option>';
      html += '    <option value="top_left"><?php echo $text_top_left; ?></option>';
      html += '    <option value="top_right"><?php echo $text_top_right; ?></option>';
EOT;
      $template_buffer = Modifier::modifyStringBuffer( $template_buffer, $search, $add, 'after', $offset, $index );      

        // call parent method
        return parent::preRender( $template_buffer, $template_name, $data );
    }
}
?>