-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: 120.25.62.241    Database: mf_mall
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `oc_zone`
--

DROP TABLE IF EXISTS `oc_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oc_zone` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_id` int(11) DEFAULT '50',
  PRIMARY KEY (`zone_id`),
  KEY `code` (`code`),
  KEY `sort_id` (`sort_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4232 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oc_zone`
--

LOCK TABLES `oc_zone` WRITE;
/*!40000 ALTER TABLE `oc_zone` DISABLE KEYS */;
INSERT INTO `oc_zone` VALUES (684,44,'安徽','AN',1,100),(685,44,'北京','BE',1,110),(686,44,'重庆','CH',1,98),(687,44,'福建','FU',1,97),(688,44,'甘肃','GA',1,96),(689,44,'广东','GU',1,108),(690,44,'广西','GX',1,94),(691,44,'贵州','GZ',1,93),(692,44,'海南','HA',1,92),(693,44,'河北','HB',1,91),(694,44,'黑龙江','HL',1,89),(695,44,'河南','HE',1,90),(697,44,'湖北','HU',1,87),(698,44,'湖南','HN',1,88),(699,44,'内蒙古','IM',1,86),(700,44,'江苏','JI',1,85),(701,44,'江西','JX',1,83),(702,44,'吉林','JL',1,84),(703,44,'辽宁','LI',1,82),(705,44,'宁夏','NI',1,81),(706,44,'陕西','SH',1,78),(707,44,'山东','SA',1,80),(708,44,'上海','SG',1,109),(709,44,'山西','SX',1,76),(710,44,'四川','SI',1,77),(711,44,'天津','TI',1,75),(712,44,'新疆','XI',1,74),(713,44,'云南','YU',1,73),(714,44,'浙江','ZH',1,107);
/*!40000 ALTER TABLE `oc_zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'mf_mall'
--

--
-- Dumping routines for database 'mf_mall'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-11 22:55:17
