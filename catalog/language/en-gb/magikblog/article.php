<?php
// Heading 
$_['heading_title']    = '类目';
$_['text_no_pq_found']     = '未找到文章';
$_['heading_blog_title']    = '客户分享';
$_['publish_at_title']    = '这篇文章是';
$_['publish_in_title']    = '属于';
$_['publish_on_title']    = '于';
$_['text_blog_by']    = 'By';
$_['text_home']    = '首页';

$_['latest_heading_title']    = '最新';
$_['text_write']               = '留下您的回复';
$_['text_login']               = '请 <a href="%s">登录</a> 或者 <a href="%s">注册</a> 以评论';
$_['text_no_comments']          = '这篇文章还没有评论.';
//$_['text_note']                = '<span class="text-danger">Note:</span> HTML is not translated!';
$_['text_note']                = '';
$_['text_success']             = '谢谢您的评论。您的评论已被提交到客服上';
$_['text_tags']             ='标签：';

// button
$_['button_continue']             ='下一步';


// Entry

$_['entry_name']               = '您的名字';
$_['entry_review']             = '您的评论';
$_['entry_rating']             = '评分';
$_['entry_good']               = '好';
$_['entry_bad']                = '坏';

// Error
$_['error_name']               = '警告: 您的名字必须在 3 到 25 个字!';
$_['error_text']               = '警告: 评论必须在 25 到 1000 字!';
$_['error_rating']             = '警告: 请选择一个评分!';
$_['error_captcha']            = '警告: 您输入的验证码与图片不符!';
$_['text_read_more']            = '浏览更多';
//$_['text_comments']            = 'Comments';
$_['text_comments']            = '评论';
//$_['text_comment']            = 'Comment';
$_['text_comment']            = '评论';
//$_['text_loading']            = 'Loading';
$_['text_loading']            = '读取中';
?> 
