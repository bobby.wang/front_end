<?php
class ControllerModuleMagikbestseller extends Controller {
	public function index($setting) {
		$this->load->language('module/magikbestseller');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['text_compare'] = $this->language->get('text_compare');
		$data['text_quickview'] = $this->language->get('text_quickview');
        $data['text_wishlist'] = $this->language->get('text_wishlist');
        $data['text_product'] = $this->language->get('text_product');
        $this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();

		$this->load->model('catalog/product');

		$this->load->model('tool/image');
		$this->load->model('catalog/product');
		$this->load->model('elasticsearch/products');

		$data['products'] = array();

		$data['magikbestseller_cbcontent']=$setting['magikbestseller_cbcontent'];
                //$results = $this->model_catalog_product->getBestSellerProducts($setting['limit']);
                //$products  = explode(",",$this->model_catalog_product->getTopicProductIDs(3)); 
                $product_id_list = preg_replace('/\s+/', '', $this->model_catalog_product->getTopicProductIDs(3));
                $products  = explode(",",$product_id_list); 
                if (!empty($products)) {	
                        $this->session->data['bestseller_productid'] = $products;
			//print_r($this->session->data['featured_productid']);exit();
			foreach ($products as $product_id) {
                            
				//save back to opencart database
				$product_info['tax_class_id']= 9;
				$product_uuid = $product_id;
				/*$product_id = $this->model_catalog_product->isUuidExist($product_id);//this step switch elastic uuid to opencart db product id
				if(!($product_id))
				{
					$product_info  = $this->model_elasticsearch_products->getProductDetail($product_uuid);
					$product_id = $this->model_catalog_product->addProduct($product_info);
				}
				$product_info = $this->model_catalog_product->getProduct($product_id);*/
                                
				//$product_info = $this->model_module_magikfeatured->getProduct($product_id);
				$es_product_info = $this->model_elasticsearch_products->getProductDetail($product_uuid);
			
                                //$product_info = $this->model_elasticsearch_products->getProductDetail($product_id);
				if ($es_product_info) {
                    if ($es_product_info['goods_main_image_url']!='' and ($es_product_info['goods_main_image_url_local'] == ''
                            or file_exists($es_product_info['goods_main_image_url_local'])==false)) {
                                //for elastic
                                if (substr($es_product_info['goods_main_image_url'], 0, 7) === "http://" || substr($es_product_info['goods_main_image_url'], 0, 8) === "https://") {
                                    $image = $es_product_info['goods_main_image_url'];
                                } else {
                                    $image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
                                }
                            }
                            elseif ( $es_product_info['goods_main_image_url_local']!='' and file_exists($es_product_info['goods_main_image_url_local'])==true){
                                $image = $es_product_info['goods_main_image_url_local'].$es_product_info[''];
                            } else {
                                $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                            }
							if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
								$price = $this->currency->format($this->tax->calculate($es_product_info['goods_current_price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
							} else {
								$price = false;
							}

							if (((float)$es_product_info['goods_current_price']<(float)$es_product_info['goods_mrsp_price'])) {
								$special = $this->currency->format($this->tax->calculate($es_product_info['goods_current_price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
							} else {
								$special = false;
							}

							if ($this->config->get('config_tax')) {
								$tax = $this->currency->format((float)$es_product_info['goods_current_price'] ? $es_product_info['goods_current_price'] : $es_product_info['goods_mrsp_price'], $this->session->data['currency']);
							} else {
								$tax = false;
							}

							/*					if ($this->config->get('config_review_status')) {
                                                    $rating = $product_info['rating'];
                                                } else {
                                                    $rating = false;
                                                }*/

							$data['products'][] = array(
								'product_id'  => $es_product_info['goods_uuid'],
								'thumb'       => $image,
								'name'        => $es_product_info['goods_title'],
								'name_cn'    	 => $es_product_info['goods_title_cn'],
								//'description' => utf8_substr(strip_tags(html_entity_decode($es_product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
								'price'       => $price,
								'special'     => $special,
								'tax'         => $tax,
								'rating'      => $es_product_info['goods_review_current_stars'],
								'href'        => $this->url->link('product/product', 'product_id=' . $es_product_info['goods_uuid']),
								//'date_end'    => $product_info['date_end']
							);
						}
					}
                        }

                $data['bestseller_list']= $this->url->link('module/magikbestseller/info');
                if ($data['products']) {
			
			return $this->load->view('module/magikbestseller', $data);
		}
	}
	
	public function info() {
	//	echo "in info";exit();
		$this->load->language('module/magikbestseller');
		$this->load->model('catalog/product');
		$this->load->model('elasticsearch/products');
		$this->load->model('tool/image');
		$heading_title_best=$this->language->get('heading_title');//.$this->language->get('text_product');

				if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = (int)$this->config->get($this->config->get('config_theme') . '_product_limit');
		}

		$this->document->setTitle($heading_title_best);

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['breadcrumbs'][] = array(
			'text' => $heading_title_best,
			'href' => $this->url->link('module/magikbestseller/info', $url)
		);
		
		$data['heading_title'] =$heading_title_best;

		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_quantity'] = $this->language->get('text_quantity');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_model'] = $this->language->get('text_model');
		$data['text_price'] = $this->language->get('text_price');
		$data['text_tax'] = $this->language->get('text_tax');
		$data['text_points'] = $this->language->get('text_points');
		$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		$data['text_sort'] = $this->language->get('text_sort');
		$data['text_limit'] = $this->language->get('text_limit');
		$data['text_quickview'] = $this->language->get('text_quickview'); 
	    $data['text_quickview'] = $this->language->get('text_quickview');
        $data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['button_list'] = $this->language->get('button_list');
		$data['button_grid'] = $this->language->get('button_grid');
		$data['button_continue'] = $this->language->get('button_continue');

		$data['compare'] = $this->url->link('product/compare');

		$data['products'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $limit,
			'limit' => $limit
		);

		$data['products'] = array();

		$this->load->model('module/magikbestseller');
//		$product_total = $this->model_module_magikbestseller->getTotalProductsBestseller();
//		//echo $product_total;exit();
//
//		//$product_total = $this->model_catalog_product->getTotalProductBestseller();
//		//echo $product_total;exit();
//		$results = $this->model_catalog_product->getBestSellerProducts($product_total);
//
//		//if ($results) {
//                    
//			foreach ($results as $result) {
//				if ($result['image']) {
//					$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
//				} else {
//					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
//				}
//
//				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
//					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
//				} else {
//					$price = false;
//				}
//
//				if ((float)$result['special']) {
//					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
//				} else {
//					$special = false;
//				}
//
//				if ($this->config->get('config_tax')) {
//					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
//				} else {
//					$tax = false;
//				}
//
//				if ($this->config->get('config_review_status')) {
//					$rating = $result['rating'];
//				} else {
//					$rating = false;
//				}
//
//				$data['products'][] = array(
//					'product_id'  => $result['product_id'],
//					'thumb'       => $image,
//					'name'        => $result['name'],					
//					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
//					'price'       => $price,
//					'special'     => $special,
//					'tax'         => $tax,
//					'rating'      => $rating,
//					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
//				);
//			}

 
			$products =$this->session->data['bestseller_productid'] ;
			$product_total=count($this->session->data['bestseller_productid'] );

			foreach ($products as $product_id) {
                            
				//save back to opencart database
				$this->load->model('catalog/product');
				$this->load->model('module/magikfeatured');
				$product_uuid = $product_id;
				$product_info['tax_class_id']= 9;
				/*$product_id = $this->model_catalog_product->isUuidExist($product_id);//this step switch elastic uuid to opencart db product id
				if(!($product_id))
				{
					$this->load->model('elasticsearch/products');
					$product_info  = $this->model_elasticsearch_products->getProductDetail($product_uuid);
					$product_id = $this->model_catalog_product->addProduct($product_info);
				}

				$product_info = $this->model_module_magikfeatured->getProduct($product_id);*/

				//echo $product_info['image'];exit();
				$es_product_info = $this->model_elasticsearch_products->getProductDetail($product_uuid);

				if ($es_product_info) {
					if ($es_product_info['goods_main_image_url']!='' and ($es_product_info['goods_main_image_url_local'] == ''
                        or file_exists($es_product_info['goods_main_image_url_local'])==false)) {
                            if (substr($es_product_info['goods_main_image_url'],0,7)==="http://" || substr($es_product_info['goods_main_image_url'],0,8)==="https://"){
                                $image = $es_product_info['goods_main_image_url'];
                            }else{
                               $image = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                            }
					}
                    elseif ($es_product_info['goods_main_image_url_local'] != '' and file_exists($es_product_info['goods_main_image_url_local'])==true) {
                        $image = $es_product_info['goods_main_image_url_local'];
                    }
                    else {
						$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
					}

					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($es_product_info['goods_current_price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$price = false;
					}

					if (((float)$es_product_info['goods_current_price']<(float)$es_product_info['goods_mrsp_price'])) {
						$special = $this->currency->format($this->tax->calculate($es_product_info['goods_current_price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$es_product_info['goods_current_price'] ? $es_product_info['goods_current_price'] : $es_product_info['goods_mrsp_price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}

/*					if ($this->config->get('config_review_status')) {
						$rating = (int)$product_info['rating'];
					} else {
						$rating = false;
					}*/
					
					$data['products'][] = array(
						'product_id'  => $es_product_info['goods_uuid'],
						'thumb'       => $image,
						'name'        => $es_product_info['goods_title'],
						'name_cn'    	 => $es_product_info['goods_title_cn'],
						//'description' => utf8_substr(strip_tags(html_entity_decode($es_product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $es_product_info['goods_review_current_stars'],
						'href'        => $this->url->link('product/product', 'product_id=' . $es_product_info['goods_uuid']),
					//'date_end'    => $product_info['date_end']
				);

				}
			}

			$url = '';

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['sorts'] = array();

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_default'),
			'value' => 'p.sort_order-ASC',
			'href'  => $this->url->link('module/magikbestseller/info', 'sort=p.sort_order&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_name_asc'),
			'value' => 'pd.name-ASC',
			'href'  => $this->url->link('module/magikbestseller/info', 'sort=pd.name&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_name_desc'),
			'value' => 'pd.name-DESC',
			'href'  => $this->url->link('module/magikbestseller/info', 'sort=pd.name&order=DESC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_price_asc'),
			'value' => 'ps.price-ASC',
			'href'  => $this->url->link('module/magikbestseller/info', 'sort=ps.price&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_price_desc'),
			'value' => 'ps.price-DESC',
			'href'  => $this->url->link('module/magikbestseller/info', 'sort=ps.price&order=DESC' . $url)
		);

		if ($this->config->get('config_review_status')) {
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_rating_desc'),
				'value' => 'rating-DESC',
				'href'  => $this->url->link('module/magikbestseller/info', 'sort=rating&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_rating_asc'),
				'value' => 'rating-ASC',
				'href'  => $this->url->link('module/magikbestseller/info', 'sort=rating&order=ASC' . $url)
			);
		}

		$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('module/magikbestseller/info', 'sort=p.model&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_model_desc'),
			'value' => 'p.model-DESC',
			'href'  => $this->url->link('module/magikbestseller/info', 'sort=p.model&order=DESC' . $url)
		);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['limits'] = array();

		$limits = array_unique(array($this->config->get('config_product_limit'), 25, 50, 75, 100));

		sort($limits);

		foreach($limits as $value) {
			$data['limits'][] = array(
				'text'  => $value,
				'value' => $value,
				'href'  => $this->url->link('module/magikbestseller/info', $url . '&limit=' . $value)
			);
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('module/magikbestseller/info', $url . '&page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

		// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
		if ($page == 1) {
		    $this->document->addLink($this->url->link('module/magikbestseller/info', '', true), 'canonical');
		} elseif ($page == 2) {
		    $this->document->addLink($this->url->link('module/magikbestseller/info', '', true), 'prev');
		} else {
		    $this->document->addLink($this->url->link('module/magikbestseller/info', 'page='. ($page - 1), true), 'prev');
		}

		if ($limit && ceil($product_total / $limit) > $page) {
		    $this->document->addLink($this->url->link('module/magikbestseller/info', 'page='. ($page + 1), true), 'next');
		}

		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['limit'] = $limit;

		$data['continue'] = $this->url->link('common/home');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
			
			
		$this->response->setOutput($this->load->view('module/magikbestseller_list', $data));

			
		//}
	}
}
