<?php
// HTTP
define('HTTP_SERVER', 'http://local.freelife.com/');

// HTTPS
define('HTTPS_SERVER', 'http://local.freelife.com/');

// DIR
define('DIR_APPLICATION', 'D:/wamp/www/freelife/catalog/');
define('DIR_SYSTEM', 'D:/wamp/www/freelife/system/');
define('DIR_IMAGE', 'D:/wamp/www/freelife/image/');
define('DIR_LANGUAGE', 'D:/wamp/www/freelife/catalog/language/');
define('DIR_TEMPLATE', 'D:/wamp/www/freelife/catalog/view/theme/');
define('DIR_CONFIG', 'D:/wamp/www/freelife/system/config/');
define('DIR_CACHE', 'D:/wamp/www/freelife/system/storage/cache/');
define('DIR_DOWNLOAD', 'D:/wamp/www/freelife/system/storage/download/');
define('DIR_LOGS', 'D:/wamp/www/freelife/system/storage/logs/');
define('DIR_MODIFICATION', 'D:/wamp/www/freelife/system/storage/modification/');
define('DIR_UPLOAD', 'D:/wamp/www/freelife/system/storage/upload/');
define('DIR_VENDOR', 'D:/wamp/www/freelife/vendor/');

//ELASTIC

define('ES_HOST', serialize(array('root:ZC054QECeJU6@47.88.87.178:9200')));
define('ES_INDEX',serialize(array('goods'=>['freelife_goods_index','goods','freelife_goods'],
                                        'description'=>['freelife_description_index','description','freelife_description'],
                                        'images'=>['freelife_image_index','image','freelife_image'],
                                        'price'=>['freelife_price_history_index','price','freelife_price_history'])));

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'freelife_new');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
