<?php  
class ControllerModuleNewsletterpopup extends Controller {

	public function index() {
	
	global $config;
		if($config->get('newsletterpopup_disp')){
			$this->load->model('tool/image');
			$this->load->model('module/newsletterpopup');
			$this->load->language('module/newsletterpopup');
			$data['newsletterpopup_bg_image'] = $this->model_tool_image->resize($config->get('newsletterpopup_bg_image'), 720,350);
			$data['newsletterpopup_title'] = $this->model_module_newsletterpopup->getNewsletterpopupTitle();
			$data['newsletterpopup_description'] = $this->model_module_newsletterpopup->getNewsletterpopupDescription();
			$data['text_subscribe'] = $this->language->get('text_subscribe');
			$data['text_dont_show'] = $this->language->get('text_dont_show');
			return $this->load->view('module/newsletterpopup', $data);
			
		}
	
	}
	

}
?>