<?php
// Heading
$_['heading_title']    = '<b style="color:red">Magik Bestsellers</b>';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified magik bestsellers module!';
$_['text_edit']        = 'Edit Bestsellers Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_limit']      = 'Limit';
$_['entry_image']      = 'Image (W x H) and Resize Type';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';
// $_['magikbestseller_cbcontent']     = 'Custom content';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify magik bestsellers module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';