<?php
// Heading
$_['heading_title']    = '<b style="color:red">Magik Featured</b>';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified magik featured module!';
$_['text_edit']        = 'Edit Featured Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_product']    = 'Products';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';
$_['magikfeatured_cbcontent']     = 'Custom content';


// Help
$_['help_product']     = '(Autocomplete)';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify magik featured module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';