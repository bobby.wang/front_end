<?php
class ModelAccountActivity extends Model {
	public function addActivity($key, $data) {
		if (isset($data['customer_id'])) {
			$customer_id = $data['customer_id'];
		} else {
			$customer_id = 0;
		}

		if (isset($_COOKIE['session_id'])){
			$session_id = $_COOKIE['session_id'];
		}else{
			$session_id = $_SESSION["default"]["token"];
			setcookie('session_id',$session_id,time() + (86400 * 30)); // 86400 = 1 day
		}

		$this->db->query("INSERT INTO `" . DB_PREFIX . "customer_activity` SET `customer_id` = '" . (int)$customer_id . "',`session_id` = '" . $session_id."' , `key` = '" . $this->db->escape($key) . "', `data` = '" . $this->db->escape(json_encode($data)) . "', `ip` = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', `date_added` = NOW()");
	}

	public function addUserActivity() {
		//log user activity
		if (isset($_COOKIE['session_id'])){
			$session_id = $_COOKIE['session_id'];
		}else{
			$session_id = session_id();
			setcookie('session_id',$session_id,time() + (86400 * 30)); // 86400 = 1 day
		}
		if (isset( $_SESSION["default"]["customer_id"])){
			$customer_name = $_SESSION["default"]["customer_id"];
		}else{
			$customer_name = "0";
		}
		$sql="INSERT INTO oc_customer_activity (customer_id,oc_customer_activity.key,customer_name,session_id,ip,url,date_added)  values (  '".$customer_name."',  'url', '". $customer_name ."' , '" . $session_id . "', '" . $_SERVER['REMOTE_ADDR'] . "', 'http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."',  NOW())";
		$this->db->query($sql);
	}
}