<?php  
class ControllerModuleNewslettersubscription extends Controller {

	public function index() {

		$this->language->load('module/newslettersubscription');

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_newsletter_subscribe'] = $this->language->get('text_newsletter_subscribe');

		
		return $this->load->view('module/newslettersubscription', $data);
		

	}
	
	public function addsubscriberemail() {

		$this->language->load('module/newslettersubscription');

		$this->load->model('module/newslettersubscription');
 		
		if(isset($this->request->get['email']))
		{
			$msg = $this->model_module_newslettersubscription->EmailSubmit(trim($this->request->get['email']));
		}
		echo $msg;
	}
}
?>