<?php
class ControllerModuleMagikbolt extends Controller {
    private $error = array(); // This is used to set the errors, if any.
 
    public function index() {
        // Loading the language file of magikbolt
        $this->load->language('module/magikbolt'); 
     
        // Set the title of the page to the heading title in the Language file i.e., Hello World
        $this->document->setTitle(strip_tags($this->language->get('heading_title')));
     
        // Load the Setting Model  (All of the OpenCart Module & General Settings are saved using this Model )
        $this->load->model('setting/setting');
     
        // Start If: Validates and check if data is coming by save (POST) method
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            // Parse all the coming data to Setting Model to save it in database.

            $this->model_setting_setting->editSetting('magikbolt', $this->request->post);
     
            // To display the success text on data save
            $this->session->data['success'] = $this->language->get('text_success');
     
            // Redirect to the Module Listing
            $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], true));
        }
     
        // Assign the language data for parsing it to view
        $data['heading_title'] = $this->language->get('heading_title');
     
        $data['text_edit']    = $this->language->get('text_edit');
        $data['text_yes']    = $this->language->get('text_yes');
        $data['text_no']    = $this->language->get('text_no');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_content_top'] = $this->language->get('text_content_top');
        $data['text_content_bottom'] = $this->language->get('text_content_bottom');      
        $data['text_column_left'] = $this->language->get('text_column_left');
        $data['text_column_right'] = $this->language->get('text_column_right');
     
        $data['entry_code'] = $this->language->get('entry_code');
        $data['entry_layout'] = $this->language->get('entry_layout');
        $data['entry_position'] = $this->language->get('entry_position');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
     
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_add_module'] = $this->language->get('button_add_module');
        $data['button_remove'] = $this->language->get('button_remove');
         
        // This Block returns the warning if any
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
     
        // This Block returns the error code if any
        if (isset($this->error['code'])) {
            $data['error_code'] = $this->error['code'];
        } else {
            $data['error_code'] = '';
        }     
     
        // Making of Breadcrumbs to be displayed on site
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], true),
            'separator' => false
        );
        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], true),
            'separator' => ' :: '
        );
        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('module/magikbolt', 'token=' . $this->session->data['token'], true),
            'separator' => ' :: '
        );
          
        $data['action'] = $this->url->link('module/magikbolt', 'token=' . $this->session->data['token'], true); // URL to be directed when the save button is pressed
     
        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], true); // URL to be redirected when cancel button is pressed
              
        // This block checks, if the hello world text field is set it parses it to view otherwise get the default 
        // hello world text field from the database and parse it
        
$config_data = array(
        
        'magikbolt_address',
        'magikbolt_phone',
        'magikbolt_email',
        'magikbolt_fb',
        'magikbolt_twitter',
        'magikbolt_gglplus',
        'magikbolt_rss',
        'magikbolt_pinterest',
        'magikbolt_linkedin',
        'magikbolt_youtube',
        'magikbolt_powerby',
        'magikbolt_home_option',
        'magikbolt_quickview_button',
        'magikbolt_scroll_totop',
        'magikbolt_sale_label',
        'magikbolt_sale_labeltitle',
        'magikbolt_sale_labelcolor',
        'magikbolt_menubar_cb',
        'magikbolt_menubar_cbtitle',
        'magikbolt_menubar_cbcontent',
        'magikbolt_vmenubar_cb',
        'magikbolt_vmenubar_cbtitle',
        'magikbolt_vmenubar_cbcontent',
        'magikbolt_product_ct',
        'magikbolt_product_cttitle',
        'magikbolt_product_ctcontent',
        'magikbolt_product_ct2',
        'magikbolt_product_ct2title',
        'magikbolt_product_ct2content',
        'magikbolt_newsletter',
        'magikbolt_newslettercontent',
        'magikbolt_footer_cb',
        'magikbolt_footer_cbcontent',
        'magikbolt_body_bg',
        'magikbolt_body_bg_ed',
        'magikbolt_fontcolor',
        'magikbolt_fontcolor_ed',
        'magikbolt_linkcolor',
        'magikbolt_linkcolor_ed',
        'magikbolt_linkhovercolor',
        'magikbolt_linkhovercolor_ed',
        'magikbolt_headerbg',
        'magikbolt_headerbg_ed',
        'magikbolt_headerlinkcolor',
        'magikbolt_headerlinkcolor_ed',
        'magikbolt_headerlinkhovercolor',
        'magikbolt_headerlinkhovercolor_ed',
        'magikbolt_headerclcolor',
        'magikbolt_headerclcolor_ed',
        'magikbolt_mmbgcolor',
        'magikbolt_mmbgcolor_ed',
        'magikbolt_mmlinkscolor',
        'magikbolt_mmlinkscolor_ed',
        'magikbolt_mmlinkshovercolor',
        'magikbolt_mmlinkshovercolor_ed',
        'magikbolt_mmslinkscolor',
        'magikbolt_mmslinkscolor_ed',
        'magikbolt_mmslinkshovercolor',
        'magikbolt_mmslinkshovercolor_ed',
        'magikbolt_buttoncolor',
        'magikbolt_buttoncolor_ed',
        'magikbolt_buttonhovercolor',
        'magikbolt_buttonhovercolor_ed',
        'magikbolt_pricecolor',
        'magikbolt_pricecolor_ed',
        'magikbolt_oldpricecolor',
        'magikbolt_oldpricecolor_ed',
        'magikbolt_newpricecolor',
        'magikbolt_newpricecolor_ed',
        'magikbolt_footerbg',
        'magikbolt_footerbg_ed',
        'magikbolt_footerlinkcolor',
        'magikbolt_footerlinkcolor_ed',
        'magikbolt_footerlinkhovercolor',
        'magikbolt_footerlinkhovercolor_ed',
        'magikbolt_powerbycolor',
        'magikbolt_powerbycolor_ed',
        'magikbolt_fonttransform',
        'magikbolt_productpage_cb',
        'magikbolt_productpage_cbcontent',
        'magikbolt_productpage_related_cb',
        'magikbolt_productpage_related_cbcontent',       
        'magikbolt_headercb_ed',
        'magikbolt_headercb_content',
        'magikbolt_footer_fb',
        'magikbolt_footer_fbcontent',
        'magikbolt_animation_effect'
        );

foreach ($config_data as $conf) {
            if (isset($this->request->post[$conf])) {
                $data[$conf] = $this->request->post[$conf];
                
            } else {
                $data[$conf] = $this->config->get($conf);

            }
        } 

 
   
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('module/magikbolt', $data));

    }

    /* Function that validates the data when Save Button is pressed */
    protected function validate() {
 
        // Block to check the user permission to manipulate the module
        if (!$this->user->hasPermission('modify', 'module/magikbolt')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
 
        /* End Block*/
 
        // Block returns true if no error is found, else false if any error detected
        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
}