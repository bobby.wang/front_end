<?php
// Text
$_['text_welcome']        = '';
$_['text_logged']         = '欢迎 <a href="%s">%s</a>';
$_['text_search'] = '点击搜索..';
$_['text_items']    = '商品';
$_['text_item']    = '商品';
$_['text_empty']    = '您的购物车为空!';
$_['text_cart']     = '查看购物车';
$_['text_checkout'] = '结账';
$_['text_recurring']  = 'Payment Profile';
$_['text_menu']        = '菜单';
$_['text_all_categories']        = '所有商品类目';
$_['text_home']        = '首页';
$_['text_blog']        = '客户分享';
$_['text_or']        = '或者';
$_['text_information']        = '相关信息';