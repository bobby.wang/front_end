<?php

/* ---------------------------------------------------------------------------------- */
/*  OpenCart ModelDecorator (used by the the Override Engine)                         */
/*                                                                                    */
/*  Copyright © 2016 by J.Neuhoff (www.mhccorp.com)                                   */
/*                                                                                    */
/*  This file is part of the Override Engine for OpenCart.                            */
/*                                                                                    */
/*  OpenCart is free software: you can redistribute it and/or modify                  */
/*  it under the terms of the GNU General Public License as published by              */
/*  the Free Software Foundation, either version 3 of the License, or                 */
/*  (at your option) any later version.                                               */
/*                                                                                    */
/*  OpenCart is distributed in the hope that it will be useful,                       */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of                    */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                     */
/*  GNU General Public License for more details.                                      */
/*                                                                                    */
/*  You should have received a copy of the GNU General Public License                 */
/*  along with OpenCart.  If not, see <http://www.gnu.org/licenses/>.                 */
/* ---------------------------------------------------------------------------------- */

class ModelDecorator extends Decorator {

	// overridden method
	protected function before( $method, $arguments ) {
		// do an event trigger before calling the method
		if ($this->startsWith( get_class($this->object), 'Model' )) {
			$route = $this->route.'/'.$method;
			$trigger = 'model/'.$this->route.'/'.$method.'/before';
			$result = $this->event->trigger($trigger, array_merge(array(&$route), $arguments));
			if ($result) {
				return $result;
			}	
		}
		return null;
	}


	// overridden method
	protected function after( $method, $arguments ) {
		// do an event trigger after having called the method
		if ($this->startsWith( get_class($this->object), 'Model' )) {
			$route = $this->route.'/'.$method;
			$trigger = 'model/'.$this->route.'/'.$method.'/after';
			$result = $this->event->trigger($trigger, array_merge(array(&$route, &$output), $arguments));
			if ($result) {
				return $result;
			}
		}
		return null;
	}
}
