<?php
class ControllerModuleCategory extends Controller {
	public function index() {
		$this->load->language('module/category');
		$this->load->model('catalog/category');

		$data['heading_title'] = $this->language->get('heading_title');

		$url='';
		if (isset($this->request->get['search_method'])) {
			$search_method= $this->request->get['search_method'];
			$url .= '&search_method=' .$search_method;
		} else {
			$search_method= 'c';
		}

		if ($search_method=='k'){

			$this->load->model('elasticsearch/products');

			if (isset($this->request->get['search'])) {
				$search = $this->model_elasticsearch_products->translate($this->request->get['search']);
				$search = htmlspecialchars_decode($search);
				$url .= '&search=' . urlencode($search);
			} else {
				$search = 'laptop';
			}

			if (isset($this->request->get['sort'])) {
				$sort = $this->request->get['sort'];
			} else {
				$sort = '';
			}

			if (isset($this->request->get['order'])) {
				$order = $this->request->get['order'];
			} else {
				$order = '';
			}

			$filter_data = array(
				'filter_name' => strtolower($search),
				'filter_category_id' => '',
				//'filter_filter'      => $filter,
				'sort'               => $sort,
				'search_method'      => $search_method,
				'order'              => $order,
				'limit'              =>1,
				'start'              =>0
			);

			$data['categories']= array();

			$results = $this->model_elasticsearch_products->getProducts($filter_data);
			$data['category_id']="103";

			$data['child_id']=0;
			foreach ($results['aggregations']['goods_category_id']['buckets'] as $category){
				if($category ['key']!="0"){
					$category_name = $this->model_catalog_category->getCategoryMdata($category['key']);
					$data['categories'][]=array(
						'category_id' => $category['key'],
						'name'        => $category_name['category_name_cn'],
						'goods_count' => $category ['doc_count'],
						'href'        => $this->url->link('product/search', 'path='. $category['key'] . $url)
					);
				}
			}

			return $this->load->view('module/related_category', $data);
		}
		else{
			if (isset($this->request->get['path'])) {
				$parts = explode('_', (string)$this->request->get['path']);
			} else {
				$parts = array();
			}

			if (isset($parts[0])) {
				$data['category_id'] = $parts[0];
			} else {
				$data['category_id'] = 0;
			}

			if (isset($parts[1])) {
				$data['child_id'] = $parts[1];
			} else {
				$data['child_id'] = 0;
			}

			if (isset($parts[2])) {
				$data['cchild_id'] = $parts[2];
			} else {
				$data['cchild_id'] = 0;
			}


			$this->load->model('catalog/product');

			$data['categories'] = array();

			$categories = $this->model_catalog_category->getCategories(0);

			foreach ($categories as $category) {
				$children_data = array();

				if ($category['category_id'] == $data['category_id']) {
					$children = $this->model_catalog_category->getCategories($category['category_id']);

					foreach($children as $child) {
						$filter_data = array('filter_category_id' => $child['category_id'], 'filter_sub_category' => true);

						$children_data[] = array(
							'category_id' => $child['category_id'],
							'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
							'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
						);
					}
				}

				$filter_data = array(
					'filter_category_id'  => $category['category_id'],
					'filter_sub_category' => true
				);

				$data['categories'][] = array(
					'category_id' => $category['category_id'],
					'name'        => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
					'children'    => $children_data,
					'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}

			return $this->load->view('module/category', $data);
		}
	}

}