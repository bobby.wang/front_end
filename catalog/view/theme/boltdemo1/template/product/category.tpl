<?php echo $header; global $config; ?>
<div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <!--
              <?php $b_i=0; $b_cnt=count($breadcrumbs); foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><?php if($b_i!=0) {?><span>/</span><?php } ?>
              <?php if($b_i==($b_cnt-1)){ ?>
              <strong><?php echo $breadcrumb['text']; ?></strong><?php } 
              else { ?>
              <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              <?php }?>
            </li>
            <?php $b_i++ ;} ?>
            -->

          </ul>
        </div>
      </div>
    </div>
</div>

<div class = "col-sm-12 col-xs-12 visible-xs">
    <div class="mobile-menu" >
        <div class="mm-search-header">
            <div id="mm_search-home">
                <div class="input-group-header">
                    <div class="input-group-btn">
                        <button id="mm-submit-button-search-header" class="btn btn-default">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                    <input id="srch-term" class="form-control simple" type="text" name="search_mm" maxlength="70" value="<?php echo $search;?>" placeholder="<?php echo $text_keyword; ?>">
                </div>
            </div>
        </div>
    </div>
</div>

<section class="main-container col2-left-layout bounceInUp animated">
<div class="main container">
  <div class="row">
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9 col-xs-12 col-sm-push-3'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <?php echo $content_top; ?>
      <article class="col-main">
      <!--<h2 class="page-heading"> <span class="page-heading-title"><?php echo $heading_title; ?></span> </h2>-->
      <?php if ($brands) { ?>
      <div class="category-list">
      <div class="row">
        <div class="col-sm-12 category-list">
          <!--<ul class="refine-search">
            <li class="filter-title"><b>Websites:</b></li>
            <?php foreach ($websites as $website) { ?>
            <li><a href="<?php echo $website['href']; ?>"><?php echo $website['website']; ?> </a> (<?php echo $website['doc_count']; ?>) </li>
            <?php } ?>
          </ul>-->
          <div class="brand-list hidden-xs">
            <li class="filter-title"><b>品牌:</b></li>
            <ul class="refine-search" id="element">
             <?php foreach ($brands as $brand) { ?>
                <li <?php if(isset($_GET['brand'])){echo 'class="selected"';} ?> ><a href="<?php echo $brand['href']; ?>">
                    <?php echo $brand['brand']; ?></a> <?php if(isset($_GET['brand'])){echo '<a href='.$remove_filter_url.' type="button">&times;</a>';}?>
                </li>
             <?php } ?>
            </ul>
              <a class="show_list pull-right"><?php echo $dropdown_more; ?></a>
          </div>
            <div class="brand-list hidden-lg hidden-md hidden-sm">
                <div class="dropdown brand col-xs-6">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" style="margin-bottom: 3px;border-radius: 0 !important;">
                        选择类目:
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1" style="height: auto;">
                        <?php foreach ($categories as $category) { ?>
                        <li role="presentation"  <?php if(isset($_GET['path'])){echo 'class="selected"';} ?> >
                        <a role="menuitem" tabindex="-1" href="<?php echo $category['href']; ?>" style="display: inline-block !important;">
                            <?php echo $category['name']; ?>(<?=$category['goods_count']?>)</a>
                        </li>
                        <?php } ?>
                        <li role="presentation" >
                        <?php if(isset($_GET['path'])){echo '<a href='.$remove_filter_url.' type="button">返回所有类目</a>';} ?>
                        </li>
                    </ul>
                </div>
                <div class="dropdown brand col-xs-6">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" style="margin-bottom: 3px;border-radius: 0 !important;">
                        选择品牌:
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1" style="height: auto;">
                        <?php foreach ($brands as $brand) { ?>
                        <li role="presentation"  <?php if(isset($_GET['brand'])){echo 'class="selected"';} ?>>
                        <a role="menuitem" tabindex="-1" href="<?php echo $brand['href']; ?>" style="display: inline-block !important;">
                            <?php echo $brand['brand']; ?></a>
                        </li>
                        <?php } ?>>
                        <li role="presentation"
                        <?php if(isset($_GET['brand'])){echo '<a href='.$remove_filter_url.' type="button">返回所有品牌</a>';}?>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="brand-list">
                <li class="filter-title"><b>价格区间:</b></li>

                <ul class="hidden-xs">
                    <?php foreach ($price_range as $range) { ?>
                    <li><a href="<?php echo $range['href']; ?>"><?php echo $range['value']; ?></a> </li><?php } ?>
                </ul>
             <li class="pull-right">
                    <form method="get" action="<?php echo $form_action; ?>">
                        <input type="hidden" name="route" value="product/search">
                        <?php if(isset($_GET['search_method'])){ ?>
                        <input type="hidden" name="search_method" value="<?php echo htmlentities($_GET['search_method']) ?>">
                        <?php } ?>
                        <?php if(isset($_GET['search'])){ ?>
                        <input type="hidden" name="search" value="<?php echo htmlentities($_GET['search']) ?>">
                        <?php } ?>
                        <?php if(isset($_GET['sort'])){ ?>
                        <input type="hidden" name="sort" value="<?php echo htmlentities($_GET['sort']) ?>">
                        <?php } ?>
                        <?php if(isset($_GET['order'])){ ?>
                        <input type="hidden" name="order" value="<?php echo htmlentities($_GET['order']) ?>">
                        <?php } ?>

                        <?php if(isset($_GET['path'])){ ?>
                        <input type="hidden" name="path" value="<?php echo htmlentities($_GET['path']) ?>">
                        <?php } ?>

                        <?php if(isset($_GET['brand'])){ ?>
                        <input type="hidden" name="brand" value="<?php echo htmlentities($_GET['brand']) ?>">
                        <?php } ?>


                        <input type="text" name="from" class="w40" value="<?php if(isset($_GET['from'])){echo htmlentities($_GET['from']);}else{echo '';}?>" />
                        <em>-</em>
                        <input type="text" name="to" class="w40" value="<?php if(isset($_GET['to'])){echo htmlentities($_GET['to']);}else{echo '';}?>" />
                        <button type="submit" class="button"><?php echo $button_search; ?></button>
                    </form>
                </li>
            </div>

        </div>
      </div>      
      </div>
      <?php } ?>
      <?php if ($products) { ?>
     
      <div class="toolbar">
        <div class="sorter">
          <div class="view-mode">
              <!--<button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
              <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>

              <a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a>-->
          </div>
        </div>

        <div id="sort-by">
          <label class="left"><?php echo $text_sort; ?></label>
          <select id="input-sort" class="form-control" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>

        <div class="pager">
          <div id="limiter">
            <label><?php echo $text_limit; ?></label>
            <select id="input-limit" class="form-control" onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
          </div>
        </div>
      </div>
      <div class="category-products">
      <!--<div class="pro_row products-list">-->
          <div class="pro_row products-grid">
        <?php foreach ($products as $product) { ?>
              <!--<div class="product-layout product-list col-xs-12">-->
              <div class="product-layout product-grid item col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="product-thumb col-item">            

      <div class="item-inner">

            <div class="item-img">
                <div class="item-img-info">
                  <span>
                  <a class="product-image" href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>">
                  <?php if ($product['thumb']) { ?>
                  <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>"/>
                  <?php } ?>
                  </a>
                  </span>
                  <?php if($config->get('magikbolt_sale_label')==1) { 
                  if ($product['price'] && $product['special']) { ?>
                  <div class="sale-label sale-top-right"><?php echo $config->get('magikbolt_sale_labeltitle'); ?></div>
                  <?php } }?>



                  <!--<div class="box-hover">
                    <ul class="add-to-links">
                    <?php if($config->get('magikbolt_quickview_button') == 1) { ?>
                    <li>
                    <a href="index.php?route=product/quickview&amp;product_id=<?php echo $product['product_id']; ?>;" class="link-quickview" data-name="<?php echo $product['name']; ?>"><?php echo $text_quickview; ?></a>
                    </li>
                    <?php  } ?> 
                    <li>
                    <a onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="link-wishlist"><?php echo $text_wishlist; ?></a> 
                    </li>
                    <li>
                    <a class="link-compare"  onclick="compare.add('<?php echo $product['product_id']; ?>');"><?php echo $button_compare; ?></a>
                    </li>

                    </ul>
                  </div>-->

                </div>
            </div>
            <div class="item-info">
                <div class="info-inner">
                    <div class="item-title"> 
                    <a title="<?php echo $product['name']; ?>" href="<?php echo $product['href']; ?>">
                    <?php echo $pro_name = (strlen($product['name'])>30) ? substr($product['name'], 0,30).'...' : $product['name']; ?>
                    </div>
                    <div id="cn_title_<?php echo $product['product_id']; ?>">
                        <?php echo $pro_name_cn = (strlen($product['name_cn'])>18) ? mb_substr(trim($product['name_cn']),0,18).'...' : $product['name_cn']; ?>
                    </div>
                    </a>
                    <?php //if ($product['rating']) { ?>
                    <div class="rating">
                    <div class="ratings">
                    <div class="rating-box">
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($product['rating'] < $i) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                    <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                    <?php } ?>
                    <?php } ?>
                    </div>
                    </div>
                    </div><!-- rating -->
                    <?php // }?>
                    <div class="desc std"><p><?php echo $product['description']; ?></p></div>
                    <div class="item-content">

                    <?php if ($product['price']) { ?>
                    <div class="item-price">
                    <div class="price-box"> 
                    <?php if (!$product['special']) { ?>
                    <p class="regular-price"><span class="price"><?php echo $product['price']; ?></span></p> 
                    <?php } else { ?> 
                    <p class="old-price"><span class="price"><?php echo $product['price']; ?></span></p>
                    <p class="special-price"><span class="price"><?php echo $product['special']; ?></span></p>                           
                    <?php } ?>
                    </div>
                    </div>
                    <?php } ?>
                    <div class="action">
                    <button type="button" title="" data-original-title="<?php echo $button_cart; ?>" class="button btn-cart link-cart" onclick="cart.add('<?php echo $product['product_id']; ?>');"><span><?php echo $button_cart; ?></span></button>
                    </div>
                    </div>

                </div>
            </div>  <!-- End Item info --> 
      </div>  <!-- End  Item inner--> 
      </div>
      </div><!-- item product-layout product-list col-xs-12 -->
        <?php } ?>
      </div>
    </div>
      <div class="row">
        <div class="bottom_pagination">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
      <?php } ?>
       </article>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
     
     </div>
       <?php echo $column_left; ?>
    <?php echo $column_right; ?></div>
</div>
 <?php echo $content_bottom; ?>
</section>
<script type="text/javascript"><!--

    $.each($(".product-image img"),function(){
        var self = $(this);
        var FitWidth = 280;
        var FitHeight = 280;
        var image = new Image();
        image.onload = function(){
            if (image.width > 0 && image.height > 0) {
                if (image.width / image.height >= FitWidth / FitHeight) {
                    if (image.width > FitWidth) {
                        self[0].width = FitWidth;
                        self[0].height = (image.height * FitWidth) / image.width
                    } else {
                        self[0].width = image.width;
                        self[0].height = image.height
                    }
                } else {
                    if (image.height > FitHeight) {
                        self[0].height = FitHeight;
                        self[0].width = (image.width * FitHeight) / image.height
                    } else {
                        self[0].width = image.width;
                        self[0].height = image.height
                    }
                }
            }
        }
        image.src = self.attr('src');
    });


    var element = document.querySelector('#element');
    if( (element.offsetHeight < element.scrollHeight)){
        // your element have overflow
        var is_shown= 0;
        $('.show_list').click(function() {
            if (is_shown==0){
                $('.show_list').html("<?php echo $dropdown_less; ?>");
                is_shown=1;
                $('.refine-search').css("overflow","visible");
                $('.refine-search').css("height", "auto");
            }else{
                $('.show_list').html("<?php echo $dropdown_more; ?>");
                is_shown=0;
                $('.refine-search').css("overflow","hidden");
                $('.refine-search').css("height", "20px");
            }
        });
    }
    else{
        //your element don't have overflow
        $('.show_list').html(" ");
    }




--></script>
<?php echo $footer; ?>

<script type="text/javascript" defer>

    <?php foreach ($products as $product) { ?>
        prod_title_translate('<?php echo $product['product_id']; ?>','19');
    <?php } ?>

</script>



