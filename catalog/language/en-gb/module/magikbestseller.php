<?php
// Heading
$_['heading_title'] = '值得买';
$_['text_quickview']  = '预览'; 
$_['text_wishlist'] = '收藏';
$_['text_product'] = 'Products';
$_['button_compare'] = '比较';
$_['button_cart'] = '加入购物车';
// Text
$_['text_tax']      = 'Ex Tax:';

// Text
$_['text_empty']        = 'There are no special offer products to list.';
$_['text_quantity']     = 'Qty:';
$_['text_manufacturer'] = 'Brand:';
$_['text_model']        = 'Product Code:';
$_['text_points']       = 'Reward Points:';
$_['text_price']        = 'Price:';
$_['text_tax']          = 'Ex Tax:';
$_['text_compare']      = 'Product Compare (%s)';
$_['text_sort']         = 'Sort By:';
$_['text_default']      = 'Default';
$_['text_name_asc']     = 'Name (A - Z)';
$_['text_name_desc']    = 'Name (Z - A)';
$_['text_price_asc']    = 'Price (Low &gt; High)';
$_['text_price_desc']   = 'Price (High &gt; Low)';
$_['text_rating_asc']   = 'Rating (Lowest)';
$_['text_rating_desc']  = 'Rating (Highest)';
$_['text_model_asc']    = 'Model (A - Z)';
$_['text_model_desc']   = 'Model (Z - A)';
$_['text_limit']        = 'Show:';