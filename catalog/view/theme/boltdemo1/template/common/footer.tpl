<?php  
 global $config; 
$facebookurl=$config->get('magikbolt_fb');
$twitterurl=$config->get('magikbolt_twitter');
$gplusurl=$config->get('magikbolt_gglplus');
$rssurl=$config->get('magikbolt_rss');
$pintresturl=$config->get('magikbolt_pinterest');
$linkedinurl=$config->get('magikbolt_linkedin');
$youtubeurl=$config->get('magikbolt_youtube');
?>

 <?php
 if($config->get('magikbolt_footer_fb')==1) {
 echo html_entity_decode($config->get('magikbolt_footer_fbcontent'));
   }
?>
<footer  class="footer bounceInUp animated">
 <div class="footer-inner">
    <div class="container">
      <div class="row">
         <div class="col-sm-12 col-xs-12 col-lg-8">
           <div class="footer-column pull-left">
              <h4><?php echo $text_service; ?></h4>
              <ul class="links">
                  <li class="first"><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                  <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
                  <!--<li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                  <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>-->
                  <!--<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>-->
                  <!--<li class="last"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>"><?php echo $text_account; ?></a></li>-->
                </ul>
           </div>
           <div class="footer-column pull-left">
              <h4><?php echo $text_extra; ?></h4>
                <ul class="links">
                    <!--<li class="first"><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>-->
                    <!--<li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>-->
                    <!--<li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>-->
                    <!--<li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>-->
                    <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                    <!--<li class="last"><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>-->
                  </ul>
           </div>
           <?php if ($informations) { ?>
           <div class="footer-column pull-left">
            <h4><?php echo $text_information; ?></h4>
            <ul class="links">
            <?php $i=0;$cnt=count($informations); foreach ($informations as $information) { ?>
            <li class="<?php if($i==0){echo 'first';} if($i==$cnt){echo 'last';} ?>"><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
            <?php $i++;} ?>
            </ul>
           </div>
           <?php } ?>
         </div>
          <div class="col-xs-12 col-lg-4">
            <div class="footer-column-last">
             
                <?php
                if($config->get('newslettersubscription_status')==1) { ?>
                    <div class="newsletter-wrap">
                      <?php echo $newslettersubscription; ?>
                      </div>
                <?php } ?>
          
            
            <div class="social">
               <h4><?php echo $text_follow_us; ?></h4>
                  <ul>
                   <?php if($facebookurl!='') { ?>
                    <li class="fb pull-left"><a href="<?php echo $facebookurl; ?>" target="_blank"></a></li>
                    <?php } ?>
                    <?php if($twitterurl!='') { ?>
                    <li class="tw pull-left"><a href="<?php echo $twitterurl; ?>" target="_blank"></a></li>
                    <?php } ?>
                    <?php if($gplusurl!='') { ?>
                    <li class="googleplus pull-left"><a href="<?php echo $gplusurl; ?>" target="_blank"></a></li>
                    <?php } ?>
                    <?php if($rssurl!='') { ?>
                    <li class="rss pull-left"><a href="<?php echo $rssurl; ?>" target="_blank"></a></li>
                    <?php } ?>
                    <?php if($pintresturl!='') { ?>
                    <li class="pintrest pull-left"><a href="<?php echo $pintresturl; ?>" target="_blank"></a></li>
                    <?php } ?>
                    <?php if($linkedinurl!='') { ?>
                    <li class="linkedin pull-left"><a href="<?php echo $linkedinurl; ?>" target="_blank"></a></li>
                    <?php } ?>
                    <?php if($youtubeurl!='') { ?>
                    <li class="youtube pull-left"><a href="<?php echo $youtubeurl; ?>" target="_blank"></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="payment-accept">
            <div>
            <img alt="payment1" src="catalog/view/theme/<?php echo $config->get('theme_default_directory') ?>/image/payment-1.png"> 
            <img alt="payment2" src="catalog/view/theme/<?php echo $config->get('theme_default_directory') ?>/image/payment-2.png"> 
            <img alt="payment3" src="catalog/view/theme/<?php echo $config->get('theme_default_directory') ?>/image/payment-3.png"> 
            <img alt="payment4" src="catalog/view/theme/<?php echo $config->get('theme_default_directory') ?>/image/payment-4.png">
            </div>
            </div>
          </div>
          </div>
    </div>
  </div>
</div>
<div class="footer-middle">
    <div class="container">
      <div class="row">

            <?php if($config->get('magikbolt_footer_cb')==1){  ?> 
             <?php echo html_entity_decode($config->get('magikbolt_footer_cbcontent')); ?> 
             <?php } ?>              
                  <address>
                      <?php if ($config->get('magikbolt_address')):?>
                      <i class="fa fa-map-marker"></i><?php echo html_entity_decode($config->get('magikbolt_address')); ?>                 
                      <?php endif;?>

                      <!--
                      <?php if ($config->get('magikbolt_phone')) : ?>

                     <i class="fa fa-mobile"></i>
                    <span> <?php echo $config->get('magikbolt_phone'); ?></span>

                      <?php endif;?>-->

                      <?php if ($config->get('magikbolt_email')): ?>
                        <i class="fa fa-envelope"></i><span>
                      <?php echo $config->get('magikbolt_email'); ?></span>
                      <?php endif; ?>
                 </address>
          </div>
         </div>
      </div>
<div class="footer-bottom">
    <div class="container">
      <div class="row">
         <?php
        if(trim($config->get('magikbolt_powerby')) != '') {
          echo html_entity_decode($config->get('magikbolt_powerby'));
        } else {
          echo $powered;
        }
      ?>        
      </div>
    </div>
</div>
</footer>
</div> <!-- page id -->

<div id="mobile-menu">
<div class="mobile-menu-inner">
  <ul>
  
      <?php if($config->get('magikbolt_home_option')==1){ ?>
      <li>
        <div class="home"><a href="<?php echo $home;?>"><i class="icon-home"></i><?php echo $text_home; ?></a></div>
      </li><?php }?>

      <?php foreach ($categories1 as $category) { ?>
      <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
        <?php if ($category['children']) { ?>
        <ul>
          <?php for($i=0;$i<count($category['children']);$i++){ ?>
          <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a>
              <?php if($category['children'][$i]['children']) { ?>
              <ul>
                <?php for($j=0;$j<count($category['children'][$i]['children']);$j++) { ?>
                <li><a href="<?php echo $category['children'][$i]['children'][$j]['href']; ?>"><?php echo $category['children'][$i]['children'][$j]['name']; ?></a></li>
                <?php }?>
                  <li></li>
              </ul>
              <?php }?> 
          </li>
            <li></li>
          <?php }?>
        </ul>
        <?php }?>
      </li>
      <?php } ?>
  </ul>
   <div class="top-links"> 
   
            <ul class="links">
            <li><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>"><?php echo $text_account; ?></a></li>
            <!--<li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><?php echo $text_wishlist; ?></a></li>-->
            <li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><?php echo $text_checkout; ?></a></li>  
            <?php if($config->get('magikblog_status')==1){ ?>
                <!--<li><a href="<?php echo $blog_href;?>"><span><?php echo $text_blog ?></span></a></li><?php }?> -->
                <li class="last">
                <?php if (!$logged) { ?>
                <a href="<?php echo $login; ?>"><?php echo $text_login; ?></a>

                <?php }  else { ?>
                <a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a>
                <?php } ?>
                </li>
                </ul>
      </div>
    </div>
    </div><!-- mobile-menu -->
<?php if($config->get('magikbolt_scroll_totop')!=1) { ?>
<script type="text/javascript">
window.onload=function(){
$('body #toTop').remove();
}
</script>
<?php }?>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->
<div id="mgkquickview">
<div id="magikloading" style="display:none;text-align:center;margin-top:400px;"><img src="catalog/view/theme/<?php echo $config->get('theme_default_directory') ?>/image/loading.gif" alt="loading">
</div></div>
</body></html>