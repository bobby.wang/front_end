<?php

/* ---------------------------------------------------------------------------------- */
/*  OpenCart Decorator (used by the the Override Engine)                              */
/*                                                                                    */
/*  Copyright © 2016 by J.Neuhoff (www.mhccorp.com)                                   */
/*                                                                                    */
/*  This file is part of the Override Engine for OpenCart.                            */
/*                                                                                    */
/*  OpenCart is free software: you can redistribute it and/or modify                  */
/*  it under the terms of the GNU General Public License as published by              */
/*  the Free Software Foundation, either version 3 of the License, or                 */
/*  (at your option) any later version.                                               */
/*                                                                                    */
/*  OpenCart is distributed in the hope that it will be useful,                       */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of                    */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                     */
/*  GNU General Public License for more details.                                      */
/*                                                                                    */
/*  You should have received a copy of the GNU General Public License                 */
/*  along with OpenCart.  If not, see <http://www.gnu.org/licenses/>.                 */
/* ---------------------------------------------------------------------------------- */

abstract class Decorator {

	protected $object;
	protected $route;


	public function __construct( $object, $route ) {
		$this->object = $object; // can be a controller or model instance
		$this->route = $route;   // route of the class, e.g. 'catalog/category'
	}


	public function __get( $key ) {
		return $this->object->__get( $key );
	}


	public function __set( $key, $val ) {
		$this->object->__set( $key, $val );
	}


	protected function before( $method, $arguments ) {
		// override this method in an extended class to implement pre-action
		return;
	}


	protected function after( $method, $arguments ) {
		// override this method in an extended class to implement post-action
		return;
	}


	public function __call( $method, $arguments ) {
	
		// make sure it's a callable method
		if (!is_callable(array($this->object, $method))) {
			trigger_error("Error: Unable to call public method '$method' in class" . get_class($this->object) . "'");
			exit;
		}

		// call pre-action 
		$this->before( $method, $arguments );

		// call the method
		$output = call_user_func_array(array($this->object, $method), $arguments);

		// call post-action
		$this->after( $method, $arguments );

		// return method output
		if ($this->startsWith( get_class($this->object), 'Controller' )) {
			if (!($output instanceof Exception)) {
				return $output;
			} else {
				return false;
			}
		} else {
			return $output;
		}
	}


	protected function startsWith( $haystack, $needle ) {
		if (strlen( $haystack ) < strlen( $needle )) {
			return false;
		}
		return (substr( $haystack, 0, strlen($needle) ) == $needle);
	}
}
