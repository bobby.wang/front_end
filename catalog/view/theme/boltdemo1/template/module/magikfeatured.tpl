<?php global $config; ?>

<?php if($products) { ?>
<style type="text/css">

#view_more_button_align {
    position: absolute;
    right:15%;
    bottom: 8%;
}
</style>
<section class="featured-pro">
  <div class="container">
    <div class="slider-items-products">
            <div class="featured-block">
            <div id="featured-slider" class="product-flexslider hidden-buttons">
                <div class="home-block-inner">
                  <!--<div class="block-title">

                     <h2><?php echo $heading_title; ?></h2>
                  </div>
                  <div class="pretext"><?php if(isset($magikfeatured_cbcontent[$config->get('config_language_id')]['magikfeatured_cbcontent'])) echo html_entity_decode($magikfeatured_cbcontent[$config->get('config_language_id')]['magikfeatured_cbcontent']); ?> </div>-->
                    <a href="<?php echo $featured_list; ?>" class="view_more_bnt" id = 'view_more_button_align'>查看全部</a>
                </div>
              <div class="slider-items slider-width-col4 products-grid block-content">
               <?php foreach ($products as $product) { ?>
                <!-- Item -->
                <div class="item">
                   <div class="item-inner">
                     <div class="item-img">
                      <div class="feature-item-img-info">
                          <span>
                                <a class="product-image" href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>">
                                <?php if ($product['thumb']) { ?>
                                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>"/>
                                <?php } ?>
                                </a>
                          </span>
                        <?php if($config->get('magikbolt_sale_label')==1) { 
                        if ($product['price'] && $product['special']) { ?>
                        <div class="sale-label sale-top-right"><?php echo $config->get('magikbolt_sale_labeltitle'); ?></div>
                        <?php } }?>

                          <!--<div class="box-hover">
                          <ul class="add-to-links">
                              <?php if($config->get('magikbolt_quickview_button') == 1) { ?>
                            <li>
                              <a class="link-quickview" href="index.php?route=product/quickview&amp;product_id=<?php echo $product['product_id']; ?>;"  data-name="<?php echo $product['name']; ?>"><?php echo $text_quickview; ?></a>
                            </li>
                             <?php  } ?> 
                            <li>
                              <a class="link-wishlist" onclick="mgk_hm_wishlist.add('<?php echo $product['product_id']; ?>');" ><?php echo $text_wishlist; ?></a> 
                            </li>
                            <li>
                              <a class="link-compare"  onclick="mgk_hm_compare.add('<?php echo $product['product_id']; ?>');"><?php echo $button_compare; ?></a>
                            </li>                       
                          </ul>
                          </div>   -->           

                    </div>
                     </div>
                      <div class="item-info">
                         <div class="info-inner">
                             <div class="item-title">
                                 <a title="<?php echo $product['name']; echo $product['name_cn']; ?>" href="<?php echo $product['href']; ?>">
                                     <?php echo $pro_name = (strlen($product['name'])>20) ? substr($product['name'], 0,20).'...' : $product['name']; ?>  <br>
                                     <?php echo $pro_name_cn = (strlen($product['name_cn'])>12) ? mb_substr($product['name_cn'], 0,12).'...' : $product['name_cn']; ?>
                                 </a>
                             </div>
                                <?php //if ($product['rating']) { ?>
                                <div class="rating">
                                  <div class="ratings">
                                    <div class="rating-box">
                                      <?php for ($i = 1; $i <= 5; $i++) { ?>
                                      <?php if ($product['rating'] < $i) { ?>
                                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                      <?php } else { ?>
                                      <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                      <?php } ?>
                                      <?php } ?>
                                    </div>
                                  </div>
                                </div><!-- rating -->
                                <?php // }?>

                            <div class="item-content">
                                
                              <?php if ($product['price']) { ?>
                               <div class="item-price">
                                <div class="price-box"> 
                                <?php if (!$product['special']) { ?>
                                <p class="regular-price"><span class="price"><?php echo $product['price']; ?></span></p> 
                                <?php } else { ?> 
                                <p class="old-price"><span class="price"><?php echo $product['price']; ?></span></p>
                                <p class="special-price"><span class="price"><?php echo $product['special']; ?></span></p>                           
                                <?php } ?>
                              </div>
                              </div>
                              <?php } ?>
                              <div class="action">
                               <button type="button" title="" data-original-title="<?php echo $button_cart; ?>" class="button btn-cart link-cart" onclick="mgk_hm_cart.add('<?php echo $product['product_id']; ?>');"><span><?php echo $button_cart; ?></span></button>
                              </div>
                            </div>

                         </div>
                      </div>  <!-- End Item info --> 
                  </div>  <!-- End  Item inner--> 
                </div> <!-- End Item --> 
                <?php } ?>
                
              </div>
            </div>    <!-- featured --> 
          </div>
    </div>    <!-- slider Item products --> 
    </div> 
</section>
<?php } ?>
<script type="text/javascript"><!--
$.each($(".feature-item-img-info img"),function(){
        var self = $(this);
        var FitWidth = 209;
        var FitHeight = 254;
        var image = new Image();
        image.onload = function(){
            if (image.width > 0 && image.height > 0) {
                if (image.width / image.height >= FitWidth / FitHeight) {
                    if (image.width > FitWidth) {
                        self[0].width = FitWidth;
                        self[0].height = (image.height * FitWidth) / image.width
                    } else {
                        self[0].width = image.width;
                        self[0].height = image.height
                    }
                } else {
                    if (image.height > FitHeight) {
                        self[0].height = FitHeight;
                        self[0].width = (image.width * FitHeight) / image.height
                    } else {
                        self[0].width = image.width;
                        self[0].height = image.height
                    }
                }
            }
        }
        image.src = self.attr('src');
 });
--></script>