<?php
class ControllerModuleSpecial extends Controller {
	public function index($setting) {
		$this->load->language('module/special');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('elasticsearch/products');

		if (isset($this->request->get['search'])) {
			$search = $this->model_elasticsearch_products->translate($this->request->get['search']);
		} else {
			$search = '';
		}
		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
			$category = array_pop($parts);
		}else{
			$category='9';
		}
		$data['products'] = array();

		$filter_data = array(
			'keyword' => $search,
			'category' => $category,
			'sort'  => 'goods_current_price',
			'order' => 'asc',
			'start' => 0,
			'limit' => $setting['limit']
		);

		$data['products'] = array();
		$results = $this->model_elasticsearch_products->getSuggestedGoods($filter_data);
		foreach ($results as $result) {
			$result['tax_class_id']=9;//9 is taxable goods,10 is download goods.

			if ((float)$result['_source']['goods_current_price']<(float)$result['_source']['goods_current_price']) {
				$special = $this->currency->format($this->tax->calculate($result['_source']['goods_current_price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$special = false;
			}
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($result['_source']['goods_current_price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$price = false;
			}

			if ($this->config->get('config_tax')) {
				$tax = $this->currency->format((float)$result['_source']['goods_current_price'] ? $result['_source']['goods_current_price'] : $result['_source']['goods_mrsp_price'], $this->session->data['currency']);
			} else {
				$tax = false;
			}
			if($result['_source']['goods_main_image_url_local']!='' and file_exists($result['_source']['goods_main_image_url_local'])==true){
				$image=$result['_source']['goods_main_image_url_local'];
			}else{
				$image=$result['_source']['goods_main_image_url'];
			}
			$data['products'][] = array(
				'product_id'  => $result['_source']['goods_uuid'],
				'thumb'       => $image,
				'name'        => utf8_substr(strip_tags(html_entity_decode($result['_source']['goods_title'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
				//'description' => utf8_substr(strip_tags(html_entity_decode($result['des'][0]['_source']['goods_description_history'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
				'price'       => $price,
				'special'     => $special,
				'tax'         => $tax,
				//'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
				//'rating'      => $result['rating'],
				'href'        => $this->url->link('product/product', 'product_id=' . $result['_source']['goods_uuid'])
			);
		}

			return $this->load->view('module/special', $data);
	}
}