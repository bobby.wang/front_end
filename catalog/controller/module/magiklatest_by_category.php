<?php
class ControllerModuleMagiklatestbycategory  extends Controller {
	
	public function index($setting) {
	

		static $module = 0;
		$this->load->language( 'module/magiklatest_by_category' );

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_tax'] = $this->language->get('text_tax');
		$data['button_cart'] = $this->language->get('button_cart');
		$data['text_quickview'] = $this->language->get('text_quickview');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_compare'] = $this->language->get('text_compare');

		$this->load->language('module/magiklatest_by_category');

		$this->load->model('catalog/product');
                $this->load->model('elasticsearch/products');
		$this->load->model('module/magiklatest_by_category');

		$this->load->model('catalog/product');
		$this->load->model('elasticsearch/products');
		$this->load->model('tool/image');

		//category wise Products
		$data['categorywise_products1'] = array();
		$data['categorywise_products2'] = array();
		$data['categorywise_products3'] = array();
		$data['categorywise_products4'] = array();
		$data['categorywise_products5'] = array();
	
		if (empty($setting['limit'])) {
			$setting['limit'] = 4;
		}

		//$data1 = array($setting['category_name1'],'limit'=> $setting['limit'],'start'=>'0');
		//echo "<pre>";print_r($data1[0]);exit();
		//$results1 = $this->model_module_magiklatest_by_category->getmagiklatestCategoryProducts($data1);
		//$results1 = [];
		$product_id_list_string = preg_replace('/\s+/', '', $this->model_catalog_product->getTopicProductIDs(2));
		$product_ID_list  = explode(",",$product_id_list_string);



		foreach ($product_ID_list as $product_uuid){

			$es_result  = $this->model_elasticsearch_products->getProductDetail($product_uuid);
			if (isset($es_result['goods_uuid'])){

				$es_result['tax_class_id']= 9;
				if ($es_result['goods_main_image_url']!='' and ($es_result['goods_main_image_url_local'] == ''
						or file_exists($es_result['goods_main_image_url_local'])==false)) {
					//for elastic
					if (substr($es_result['goods_main_image_url'],0,7)==="http://" || substr($es_result['goods_main_image_url'],0,8)==="https://"){
						$image = $es_result ['goods_main_image_url'];
					}else{
						$image = $this->model_tool_image->resize($es_result ['goods_main_image_url'], $setting['width'], $setting['height']);
					}
				}
				elseif ($es_result['goods_main_image_url_local']!='' and file_exists($es_result['goods_main_image_url_local'])==true){
					$image = $es_result ['goods_main_image_url_local'];
				}else {
					$image = false;
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($es_result ['goods_current_price'], $es_result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$es_result ['goods_current_price']<(float)$es_result ['goods_mrsp_price']) {
					$special = $this->currency->format($this->tax->calculate($es_result ['goods_current_price'], $es_result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				/*if ($this->config->get('config_review_status')) {
                    $rating = $result['rating'];
                } else {
                    $rating = false;
                }*/


				$data['categorywise_products1'][] = array(
						'product_id' => $es_result['goods_uuid'],
						'product_uuid' => $es_result['goods_uuid'],
						'thumb'   	 => $image,
						'name'    	 => $es_result['goods_title'],
					    'name_cn'    	 => $es_result['goods_title_cn'],
						'price'   	 => $price,
						'special' 	 => $special,
						'rating'     => $es_result['goods_review_current_stars'],
					//'reviews'    => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
						'href'    	 => $this->url->link('product/product', 'product_id=' . $es_result['goods_uuid']),
				);

			}

		}

		//$data2 = array($setting['category_name2'],'limit'=> $setting['limit'],'start'=>'0');
		//echo "<pre>";print_r($data2);exit();
		//$results2 = $this->model_module_magiklatest_by_category->getmagiklatestCategoryProducts($data2);
		//$results2 = [];
		$product_id_list_string = preg_replace('/\s+/', '', $this->model_catalog_product->getTopicProductIDs(10));
		$product_ID_list  = explode(",",$product_id_list_string);
		$max_product=4;
		$i=0;
		foreach ($product_ID_list as $product_uuid){
			//save back to opencart database
			$i++;
			if ($i>$max_product){
				break;
			}
			$es_result1  = $this->model_elasticsearch_products->getProductDetail($product_uuid);
			if (isset($es_result1['goods_uuid'])){
				$es_result1['tax_class_id']= 9;
				if ($es_result1['goods_main_image_url']!='' and ($es_result1['goods_main_image_url_local'] == ''
						or file_exists($es_result1['goods_main_image_url_local'])==false)) {
					if (substr($es_result1['goods_main_image_url'],0,7)==="http://" || substr($es_result1['goods_main_image_url'],0,8)==="https://"){
						$image = $es_result1['goods_main_image_url'];
					}else{
						$image = $this->model_tool_image->resize($es_result1['goods_main_image_url'], $setting['width'], $setting['height']);
					}
				}
				elseif ($es_result1['goods_main_image_url_local']!='' and file_exists($es_result1['goods_main_image_url_local'])==true){
					$image = $es_result1 ['goods_main_image_url_local'];
				} else {
					$image = false;
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($es_result1['goods_current_price'], $es_result1['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$es_result1['goods_current_price']<(float)$es_result1['goods_mrsp_price']) {
					$special = $this->currency->format($this->tax->calculate($es_result1['goods_current_price'], $es_result1['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				/*if ($this->config->get('config_review_status')) {
                    $rating = $result['rating'];
                } else {
                    $rating = false;
                }*/


				$data['categorywise_products2'][] = array(
						'product_id' => $es_result1['goods_uuid'],
						'product_uuid' => $es_result1['goods_uuid'],
						'thumb'   	 => $image,
						'name'    	 => $es_result1['goods_title'],
					    'name_cn'    	 => $es_result1['goods_title_cn'],
						'price'   	 => $price,
						'special' 	 => $special,
						'rating'     => $es_result1['goods_review_current_stars'],
					//'reviews'    => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
						'href'    	 => $this->url->link('product/product', 'product_id=' . $es_result1['goods_uuid']),
				);
			}

		}








		//get name of category
		$this->load->model('catalog/category');
		$category_result1 = $this->model_catalog_category->getCategory($setting['category_name1']);	
		$category_result2 = $this->model_catalog_category->getCategory($setting['category_name2']);	
		$category_result3 = $this->model_catalog_category->getCategory($setting['category_name3']);		
		$category_result4 = $this->model_catalog_category->getCategory($setting['category_name4']);		
		//$category_result5 = $this->model_catalog_category->getCategory($setting['category_name5']);		
		$data['category_name1'] = NULL;
		if(isset($category_result1['name']))
		$data['category_name1'] = $category_result1['name'];
		$data['category_name2'] = NULL;
		if(isset($category_result2['name']))
		$data['category_name2'] = $category_result2['name'];
		$data['category_name3'] = NULL;
		if(isset($category_result3['name']))
		$data['category_name3'] = $category_result3['name'];
		$data['category_name4'] = NULL;
		if(isset($category_result4['name']))
		$data['category_name4'] = $category_result4['name'];
		// $data['category_name5'] = NULL;
		// if(isset($category_result5['name']))
		// $data['category_name5'] = $category_result5['name'];

		$data['heading_title'] = $this->language->get('heading_title');
		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['no_product_found'] = $this->language->get('no_product_found');

		$data['module'] = $module++;

		
		return $this->load->view('module/magiklatest_by_category', $data);
		

		/*$this->render();*/
	}
}
?>