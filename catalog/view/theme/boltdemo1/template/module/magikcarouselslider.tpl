<div class="wow bounceInUp custom-slider">
    <div>
         <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
          <ol class="carousel-indicators">
            <?php $cnt1 = 0; foreach ($slider as $slide) { ?>
              <li data-target="#carousel-example-generic" data-slide-to="<?php echo $cnt1 ?>" class="active"></li>
              <?php $cnt1++;}?>
            </ol>
           
                <?php if($slider) { ?>
                <div class="carousel-inner" role="listbox">
                  <?php $i=0; foreach($slider as $slide){ ?>   
                  
                  <div class="item <?php if($i==0) echo 'active';?>"> 
                    <a href="<?php echo $slide['link'];?>" target="_blank" title="<?php if($slide['title']) {echo $slide['title'];}?>">
                    <img  src="<?php echo $slide['image']?>" data-bgposition='left top'  data-bgfit='cover' data-bgrepeat='no-repeat' title="<?php if($slide['title']) {echo $slide['title'];}?>" alt="<?php if($slide['title']) {echo $slide['title'];}?>"/>
                    </a>
                    <div class="carousel-caption">
                      <h3>
                        <a href="<?php echo $slide['link'];?>" target="_blank" title="<?php if($slide['title']) {echo $slide['title'];}?>">
                          <?php if($slide['title']) {echo $slide['title'];}?>
                        </a>
                      </h3>
                      <p><?php if($slide['description']) { echo $slide['description'];}?></p>
                    </div>
                  </div>

                <?php $i++; } ?>
                </div>
                <?php } ?>
           
             
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> 
         </div>
    </div>
</div>