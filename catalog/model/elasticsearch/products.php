<?php
/**
 * Created by PhpStorm.
 * User: Bill
 * Date: 2016-07-14
 * Time: 11:36 PM
 */
require (DIR_VENDOR.'autoload.php');
use Elasticsearch\ClientBuilder;
class ModelElasticsearchProducts extends Model{

    private $hosts;
    private $config;
    private $elasticsearch;

    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->hosts = unserialize(ES_HOST);
        $this->config = unserialize(ES_INDEX);
        $this->elasticsearch = ClientBuilder::create()->setHosts($this->hosts)->build();
        $this->google_key = 'AIzaSyAeSQFrACMCWAZ0YubXlXc5Wrdj6Oiwb2g';
        $this->translateURL = 'https://www.googleapis.com/language/translate/v2?key='.$this->google_key.'&target=en';
        $this->detectURL = 'https://www.googleapis.com/language/translate/v2/detect?key=AIzaSyAeSQFrACMCWAZ0YubXlXc5Wrdj6Oiwb2g';


    }


    public function translate($query)
    {

        //检测是否有中文,如果没有则直接返回

        if (!preg_match("/([\x81-\xfe][\x40-\xfe])/", $query)) {
            return $query;
        }
        $url="http://api.malama.ca/translate.php?q=".urlencode($query);
        $url.='&source=zh-CN';
        $url.='&target=en';
        $html=file_get_contents($url);
        $response=json_decode($html,true);
        if(strlen($response['keyword'])>0){
            return $response['keyword'];
        }
        return $query;

    }

    public function google_translate($keyword){

        //检测是否有中文,如果没有则直接返回
        if (!preg_match("/([\x81-\xfe][\x40-\xfe])/", $keyword)) {
            return $keyword;
        }
        //启用Google translate
        $key='AIzaSyDtJEjoRXAJlKJsKxBCERJeMneB3ikf6K4';
        $url='https://www.googleapis.com/language/translate/v2?key='.$key.'&source=zh-CN&target=en&q=';
        $text_encode=urlencode($keyword);//urlencode(mb_convert_encoding($text, 'utf-8', 'gb2312'));
        $url.=$text_encode;
        $html=file_get_contents($url);
        $arr=json_decode($html,true);
        $translatedText=$arr['data']['translations'][0]['translatedText'];
        return $translatedText;
    }

    public function update_db($text, $goods_uuid)
    {
        $this->db->query("UPDATE `freelife_mdata_goods` SET seller_product_title_cn='".$text."' WHERE seller_product_uuid='".$goods_uuid."'");
    }

    public function translate_reverse($text, $language = 'en|zh-cn')
    {

        $url="http://api.malama.ca/translate.php?q=".urlencode($text);
        $url.='&source=en';
        $url.='&target=zh-CN';
        $html=file_get_contents($url);
        $arr = json_decode($html, true);
        $translatedText = $arr['keyword'];
        return $translatedText;
    }

    public function check_price_api($prod_url){
        $array["url"] = $prod_url;
        $array["key"] = "92b4bf7bdaf04048b8855480124a49af";
        $data = json_encode($array);
        $price_output = $this->do_curl_get_request_price("http://api.bigdataapi.com:20008/retriver/price",$data,30);
        $price_output = json_decode($price_output, true);
        if (isset($price_output['price'])) {
            $price = $price_output['price'];
        }
        else{
            $price = "-1.00";
        }
        return $price;
    }

    private function do_curl_get_request_price($url, $data, $timeout = 30){
        $headers=array('Accept: application/json','Content-Type: application/json');
        if ($url == '' || $timeout <= 0) {
        return false;
        }
        if ($data != null) {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //将curl_exec()获取的信息以文件流的形式返回，而不是直接输出
            curl_setopt($ch, CURLOPT_HEADER, False); //启用时会将头文件的信息作为数据流输出
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout); //设置cURL允许执行的最长秒数
            $output = curl_exec($ch);
        }
        if ($output === FALSE) {
            #echo "cURL Error: " . curl_error($ch);
            return false;
        }
        return $output;
    }

    private function do_curl_get_request($url, $data, $timeout = 30){
        if ($url == '' || $timeout <= 0) {
            return false;
        }
        if ($data != null) {
//            $url = $url . '&q=' . http_build_query($data);
             $url = $url . '&q=' . urlencode($data);
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //将curl_exec()获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_HEADER, false); //启用时会将头文件的信息作为数据流输出
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout); //设置cURL允许执行的最长秒数
        $output = curl_exec($ch);
        if ($output === FALSE) {
            #echo "cURL Error: " . curl_error($ch);
            return false;
        }
        return $output;
    }

    public function getProducts($condition)
    {
        $params = array();
        //$keyword_cn= '';
        $keyword_en = '';

        if (!empty($condition['filter_name'])) {
            $keyword_en = $condition['filter_name'];
            //$keyword_en=$this->translate($keyword);
        }
        $params['index'] = $this->config['goods'][0];
        $query = [];
        $filter = [];
        //关键词
        if (isset($keyword_en) && strlen($keyword_en) > 0) {
            $keyword_en_arr = preg_split('/\s+/', $keyword_en);
            if (count($keyword_en_arr) < 7) {
                foreach ($keyword_en_arr as $k => $v) {
                    $query['must'][] = [
                        'wildcard' => [
                            'goods_search' => "*|||".$v."|||*"
                        ]
                    ];
                }
            } else {
                foreach ($keyword_en_arr as $k => $v) {
                    $query['must'][] = [
                        'query_string' => [
                            'default_field' => 'goods_search',
                            'query' => $v
                        ]
                    ];
                }
            }
            $filter['must'][] = [
                'term' => [
                    'goods_status' => 1
                ]
            ];
            $filter['must_not'][] = [
                'term' => [
                    'goods_category_id' => 9
                ]
            ];
        }

        //主类目
       if (isset($condition['filter_category_id'])&& intval($condition['filter_category_id'])!=9) {
           if (is_array($condition['filter_category_id'])) {
               if ($condition['filter_name'] == ''){
                   $filter['must'][] = [
                       'terms' => [
                           'goods_category_id' => $condition['filter_category_id']
                       ]
                   ];
               }else{
                   $category_query = $this->model_catalog_category->getMdataCategoryQuery($condition['filter_category_id']);

               }

           } else {
               if (intval($condition['filter_category_id']) > 0) {
                   if ($condition['filter_name'] != ''){
                       $filter['must'][] = [
                           'term' => [
                               'goods_category_id' => $condition['filter_category_id']
                           ]
                       ];
                   }else{
                       $query = $this->model_catalog_category->getMdataCategoryQuery($condition['filter_category_id']);
                       if (!empty($query)) {
                           $query1=strval($query['mdata_es_sql']);
                           $json_obj = json_decode($query1, true);
                           $params['body']['query']['filtered']['query']['bool'] = $json_obj;
//                           $params['body']= [
//                               'query' => [
//                                   'filtered'=>[
//                                       'query' =>[
//                                           'bool' => $query1
//                                                  ]
//                                               ]
//                                        ]
//                             ];
                       }
                   }
//                   $filter['must'][] = [
//                       'prefix' => [
//                           'goods_category_id' => $condition['filter_category_id']
//                       ]
//                   ];
               }
           }
           $filter['must'][] = [
               'term' => [
                   'goods_status' => 1
               ]
           ];
       }

        //起始价格
        if(!empty($condition['price_start'])){
            $filter['must'][]=[
                'range'=>[
                    'goods_current_price'=>[
                        'gte'=>$condition['price_start']
                    ]
                ]
            ];
        }
        //最大价格
        if(isset($condition['price_end'])&&$condition['price_end']>0){
            $filter['must'][]=[
                'range'=>[
                    'goods_current_price'=>[
                        'lte'=>$condition['price_end']
                    ]
                ]
            ];
        }
        //品牌
        if(!empty($condition['brand'])){
            $filter['must'][]=[
                'term'=>[
                    'goods_brand'=>$condition['brand']
                ]
            ];
        }
        //采集站点
        if(!empty($condition['website'])){
            $filter['must'][]=[
                'term'=>[
                    'goods_website'=>$condition['website']
                ]
            ];
        }


        if ($condition['filter_name'] != '')
            if (!empty($query)) {
                $params['body']['query']['filtered']['query']['bool'] = $query;
            }
        if (!empty($filter)) {
            $params['body']['query']['filtered']['filter']['bool'] = $filter;
        }

        if ($condition['search_method']=='k'&& $condition['sort']==''){
            $params['body']['sort'] = [
                '_score' => [
                    'order' => 'desc'
                ],
                'goods_order_score' =>[
                    'order' => 'desc'
                ]
            ];
        }
        else{
            $params['body']['sort'] = [
                'goods_order_score' =>[
                    'order' => 'desc'
                ]
            ];
        }
        //商品排序
        if (!empty($condition['order'])) {
            $params['body']['sort'] = [
                $condition['sort'] => [
                    'order' => $condition['order']
                ]
            ];
        }

        //搜索分页
        //$this->size = array_key_exists('size', $condition) ? $condition['size'] : $this->size;
        $params['body']['size'] = $condition['limit'];
        //$page = array_key_exists('page', $condition) ? $condition['page'] : 0;
        $params['body']['from'] = $condition['start'];
        //对采集站点和品牌进行分组统计
        $params['body']['aggs'] = [
            'goods_website' => [
                'terms' => [
                    'field' => 'goods_website',
                    'size' => 10,
                    'order' => [
                        "_count" => "desc"
                    ]
                ]
            ],
            'goods_brand' => [
                'terms' => [
                    'field' => 'goods_brand',
                    'size' => 20,
                    'order' => [
                        "_count" => "desc"
                    ]
                ]
            ],
            'goods_category_id' => [
                'terms' => [
                    'field' => 'goods_category_id',
                    'size' => 10,
                    'order' => [
                        "_count" => "desc"
                    ]
                ]
            ]
        ];

        //Topic Json 字符串
        if (!empty($condition['json'][0])) {
            $topic_json = $condition['json'][0]['condition'];
            $params['body'] = $topic_json;
        }

        if ($condition['filter_name'] != '') {
            $final_json = json_encode($params);
            //echo $final_json;
            $params = json_decode($final_json, true);
        }
        //开始在elasticsearch中进行搜索
        $results = $this->elasticsearch->search($params);
        //translate and update
        $count = count($results['hits']['hits']);
        //for ($i = 0;$i < $count;$i++){
            //if ($results['hits']['hits'][$i]['_source']['goods_title_cn'] == '') {
                //$results['hits']['hits'][$i]['_source']['goods_title_cn'] = $this->translate_reverse($results['hits']['hits'][$i]['_source']['goods_title']);
                //$this->update_db($results['hits']['hits'][$i]['_source']['goods_title_cn'], $results['hits']['hits'][$i]['_source']['goods_uuid']);
            //}
        //}
        return $results;
     }

    public function getProductDetail($goods_uuid){

        $params['index'] = $this->config['goods'][0];

        if (isset($goods_uuid)) {

            $query['must'][] = [
                'term' => [
                    'goods_uuid' => $goods_uuid
                ]
            ];
        }

        if (!empty($query)) {
            $params['body']['query']['bool'] = $query;
        }

        $results = $this->elasticsearch->search($params);
//        $count = count($results['hits']['hits']);
        //translate and update
        //for ($i = 0;$i < $count;$i++){
            //if ($results['hits']['hits'][$i]['_source']['goods_title_cn'] == '') {
                //$results['hits']['hits'][$i]['_source']['goods_title_cn'] = $this->translate_reverse($results['hits']['hits'][$i]['_source']['goods_title']);
                //$this->update_db($results['hits']['hits'][$i]['_source']['goods_title_cn'], $results['hits']['hits'][$i]['_source']['goods_uuid']);
           // }
        //}
        if ($results['hits']['total']>0) {
            return $results['hits']['hits'][0]['_source'];
        } else {
            return null;
        }

    }

    public function getProductDescription($goods_uuid){

        $params['index'] = $this->config['description'][0];

        if (isset($goods_uuid)) {

                $query['must'][] = [
                    'term' => [
                        'goods_uuid' => $goods_uuid
                    ]
                ];
        }

        if (!empty($query)) {
            $params['body']['query']['bool'] = $query;
        }

        $results = $this->elasticsearch->search($params);
        return $results['hits']['hits'];
    }

    public function getProductImages($goods_uuid){

        $params['index'] = $this->config['images'][0];

        if (isset($goods_uuid)) {

            $query['must'][] = [
                'term' => [
                    'goods_uuid' => $goods_uuid
                ]
            ];
        }

        if (!empty($query)) {
            $params['body']['query']['bool'] = $query;
        }

        $results = $this->elasticsearch->search($params);
        return $results['hits']['hits'];
    }

    public function getProductPrice($goods_uuid){

        $params['index'] = $this->config['price'][0];

        if (isset($goods_uuid)) {

            $query['must'][] = [
                'term' => [
                    'goods_uuid' => $goods_uuid
                ]
            ];
        }

        if (!empty($query)) {
            $params['body']['query']['bool'] = $query;
        }

        $results = $this->elasticsearch->search($params);
        return $results['hits']['hits'];
    }

    public function getProductVariation($product_uuid){

        $params['index'] = $this->config['goods'][0];

        if (isset($product_uuid)) {

            $query['must'][] = [
                'term' => [
                    'product_uuid' => $product_uuid
                ]
            ];
        }

        if (!empty($query)) {
            $params['body']['query']['bool'] = $query;
        }

        $results = $this->elasticsearch->search($params);
        return $results['hits']['hits'];
    }

    public function getSuggestedGoods($filter_data){

        $params['index'] = $this->config['goods'][0];

        if (isset($filter_data['keyword']) && strlen($filter_data['keyword']) > 0) {
            $filter_data['keyword'] = htmlspecialchars_decode(strtolower($filter_data['keyword']));
            $keyword_en_arr = preg_split('/\s+/', $filter_data['keyword']);
            if (count($keyword_en_arr) < 7) {
                foreach ($keyword_en_arr as $k => $v) {
                    $query['must'][] = [
                        'wildcard' => [
                            'goods_search' => "*|||".$v."|||*"
                        ]
                    ];
                }
            } else {
                foreach ($keyword_en_arr as $k => $v) {
                    $query['must'][] = [
                        'query_string' => [
                            'default_field' => 'goods_search',
                            'query' => $v
                        ]
                    ];
                }
            }
            $filter['must'][] = [
                'term' => [
                    'goods_status' => 1
                ]
            ];
            $filter['must_not'][] = [
                'term' => [
                    'goods_category_id' => 9
                ]
            ];
        }

        if (isset($filter_data['category'])&& intval($filter_data['category'])!=9) {
             $specialquery = $this->model_catalog_category->getMdataCategoryQuery($filter_data['category']);
            if (!empty($specialquery)) {
                $query1=strval($specialquery['mdata_es_sql']);
                $json_obj = json_decode($query1, true);
                $params['body']['query']['filtered']['query']['bool'] = $json_obj;
            }

            $filter['must'][] = [
                'term' => [
                    'goods_status' => 1
                ]
            ];
        }

        $params['body']['sort'] = [
            $filter_data['sort'] => [
                'order' => $filter_data['order']
            ]
        ];

        $params['body']['size'] = $filter_data['limit'];

        if (!empty($query)) {
            $params['body']['query']['filtered']['query']['bool'] = $query;
        }
        if (!empty($filter)) {
            $params['body']['query']['filtered']['filter']['bool'] = $filter;
        }

        $final_json = json_encode($params);

        $results = $this->elasticsearch->search($params);
        return $results['hits']['hits'];
    }

}
