<?php echo $header; global $config; ?>
<div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <?php $b_i=0; $b_cnt=count($breadcrumbs); foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><?php if($b_i!=0) {?><span>/</span><?php } ?>
              <?php if($b_i==($b_cnt-1)){ ?>
              <strong><?php echo $breadcrumb['text']; ?></strong><?php } 
              else { ?>
              <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              <?php }?>
            </li>
            <?php $b_i++ ;} ?>

          </ul>
        </div>
      </div>
    </div>
  </div>
<div class="main-container">
  <div class="main container">
    <div class="">
  <div class="row"><?php echo $column_left;  ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
       <div class="col-main">
      <?php echo $content_top; ?>
      <div class="page-title"><h2><?php  echo $heading_title; ?></h2></div>
      <?php if ($products) { ?>
      <div class="toolbar">
        <div class="sorter">
          <div class="view-mode">
            <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
            <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
            
            <!--<a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a>-->
          </div>
        </div>

        

      </div><!-- toolbar -->

  <div class="category-products">
      <div class="pro_row products-list">
        <?php foreach ($products as $product) { ?>
   <div class="item product-layout product-list col-xs-12">
          <div class="product-thumb col-item">
              <!-- <div class="item"> -->
                    <div class="item-inner">

                     <div class="item-img">
                      <div class="feature-list-item-img-info">
                          <span>
                                <a class="product-image" href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>">
                                <?php if ($product['thumb']) { ?>
                                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>"/>
                                <?php } ?>
                                </a>
                          </span>
<?php if($config->get('magikbolt_sale_label')==1) { 
                        if ($product['price'] && $product['special']) { ?>
                        <div class="sale-label sale-top-right"><?php echo $config->get('magikbolt_sale_labeltitle'); ?></div>
                        <?php } }?>



                      <!--<div class="box-hover">
                      <ul class="add-to-links">

                          <?php if($config->get('magikbolt_quickview_button') == 1) { ?>
                        <li>
                          <a href="index.php?route=product/quickview&amp;product_id=<?php echo $product['product_id']; ?>;" class="link-quickview" data-name="<?php echo $product['name']; ?>"><?php echo $text_quickview; ?></a>
                        </li>
                         <?php  } ?> 
                        <li>
                          <a onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="link-wishlist"><?php echo $text_wishlist; ?></a> 
                        </li>
                        <li>
                            <a class="link-compare"  onclick="compare.add('<?php echo $product['product_id']; ?>');"><?php echo $button_compare; ?></a>
                        </li>
                       
                      </ul>
                    </div>-->

                      </div>
                     </div>
                      <div class="item-info">
                         <div class="info-inner">
                            <div class="item-title"> 
                              <a title="<?php echo $product['name']; ?>" href="<?php echo $product['href']; ?>">
                              <?php echo $pro_name = (strlen($product['name'])>20) ? substr($product['name'], 0,20).'...' : $product['name']; ?>
                              </a>
                            </div>
                             <div id="cn_title_<?php echo $product['product_id']; ?>">
                                 <?php echo $pro_name_cn = (strlen($product['name_cn'])>20) ? mb_substr(trim($product['name_cn']),0,20).'...' : $product['name_cn']; ?>
                             </div>
                                                        <?php //if ($product['rating']) { ?>
                                <div class="rating">
                                  <div class="ratings">
                                    <div class="rating-box">
                                      <?php for ($i = 1; $i <= 5; $i++) { ?>
                                      <?php if ($product['rating'] < $i) { ?>
                                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                      <?php } else { ?>
                                      <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                      <?php } ?>
                                      <?php } ?>
                                    </div>
                                  </div>
                                </div><!-- rating -->
                                <?php // }?>
                            <!--<div class="desc std"><p><?php echo $product['description']; ?></p></div>-->
                            <div class="item-content">
                                
                              <?php if ($product['price']) { ?>
                               <div class="item-price">
                                <div class="price-box"> 
                                <?php if (!$product['special']) { ?>
                                <p class="regular-price"><span class="price"><?php echo $product['price']; ?></span></p> 
                                <?php } else { ?> 
                                <p class="old-price"><span class="price"><?php echo $product['price']; ?></span></p>
                                <p class="special-price"><span class="price"><?php echo $product['special']; ?></span></p>                           
                                <?php } ?>
                              </div>
                              </div>
                              <?php } ?>
                              <div class="action">
                               <button type="button" title="" data-original-title="<?php echo $button_cart; ?>" class="button btn-cart link-cart" onclick="cart.add('<?php echo $product['product_id']; ?>');"><span><?php echo $button_cart; ?></span></button>
                              </div>
                            </div>

                         </div>
                      </div>  <!-- End Item info --> 
                  </div>  <!-- End  Item inner--> 
              <!-- </div> --><!--  Item --> 

          </div>
        </div><!-- item product-layout product-list col-xs-12 -->
        <?php } ?>
      </div></div>

      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class=""><a href="<?php echo $continue; ?>" class=""><button class="button continue"><?php echo $button_continue; ?></button></a></div>
      </div>
      <?php } ?>
      </div></div>
    <?php echo $column_right; ?></div>
</div>
</div>
</div><?php echo $content_bottom; ?>

<script type="text/javascript"><!--
$.each($(".feature-list-item-img-info img"),function(){
        var self = $(this);
        var FitWidth = 285;
        var FitHeight = 345;
        var image = new Image();
        image.onload = function(){
            if (image.width > 0 && image.height > 0) {
                if (image.width / image.height >= FitWidth / FitHeight) {
                    if (image.width > FitWidth) {
                        self[0].width = FitWidth;
                        self[0].height = (image.height * FitWidth) / image.width
                    } else {
                        self[0].width = image.width;
                        self[0].height = image.height
                    }
                } else {
                    if (image.height > FitHeight) {
                        self[0].height = FitHeight;
                        self[0].width = (image.width * FitHeight) / image.height
                    } else {
                        self[0].width = image.width;
                        self[0].height = image.height
                    }
                }
            }
        }
        image.src = self.attr('src');
 });
--></script>
<?php echo $footer; ?>
<script type="text/javascript" defer>

    <?php foreach ($products as $product) { ?>
        prod_title_translate('<?php echo $product['product_id']; ?>','19');
    <?php } ?>

</script>
