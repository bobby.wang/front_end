<?php

/* ---------------------------------------------------------------------------------- */
/*  OpenCart library class Language (with modififications for the Override Engine)    */
/*                                                                                    */
/*  Original file Copyright © 2016 by Daniel Kerr (www.opencart.com)                  */
/*  Modifications Copyright © 2016 by J.Neuhoff (www.mhccorp.com)                     */
/*                                                                                    */
/*  This file is part of OpenCart.                                                    */
/*                                                                                    */
/*  OpenCart is free software: you can redistribute it and/or modify                  */
/*  it under the terms of the GNU General Public License as published by              */
/*  the Free Software Foundation, either version 3 of the License, or                 */
/*  (at your option) any later version.                                               */
/*                                                                                    */
/*  OpenCart is distributed in the hope that it will be useful,                       */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of                    */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                     */
/*  GNU General Public License for more details.                                      */
/*                                                                                    */
/*  You should have received a copy of the GNU General Public License                 */
/*  along with OpenCart.  If not, see <http://www.gnu.org/licenses/>.                 */
/* ---------------------------------------------------------------------------------- */

class Language {
	private $default = 'en-gb';
	private $directory;
	private $data = array();
	private $factory = null;

	public function __construct($directory = '', $factory = null) {
		if ($factory) {
			// caller has provided the Override Engine factory
			$this->factory = $factory;
		} else {
			// try to find the Override Engine factory from the calling object
			$object = null;
			$backtrace = debug_backtrace();
			if (!empty($backtrace[1]['object'])) {
				$object = $backtrace[1]['object'];
			}
			if (!empty($object->factory)) {
				$this->factory = $object->factory;
			}
		}

		$this->directory = $directory;
	}

	public function get($key) {
		return (isset($this->data[$key]) ? $this->data[$key] : $key);
	}
	
	public function set($key, $value) {
		$this->data[$key] = $value;
	}
	
	// Please dont use the below function the Opencart project is thinking getting rid of it.
	public function all() {
		return $this->data;
	}
	
	// Please dont use the below function the Opencart project is thinking getting rid of it.
	public function merge(&$data) {
		array_merge($this->data, $data);
	}
			
	public function load($filename, &$data = array()) {
		if (!empty($this->factory)) {
			$_ = $this->factory->loadLanguage($filename);
			$this->data = array_merge( $this->data, $_ );
			return $this->data;
		}

		$_ = array();

		$file = DIR_LANGUAGE . 'english/' . $filename . '.php';
		
		if (is_file($file)) {
			require($file);
		}

		$file = DIR_LANGUAGE . $this->default . '/' . $filename . '.php';

		if (is_file($file)) {
			require($file);
		}

		$file = DIR_LANGUAGE . $this->directory . '/' . $filename . '.php';

		if (is_file($file)) {
			require($file);
		}

		$this->data = array_merge($this->data, $_);

		return $this->data;
	}
}