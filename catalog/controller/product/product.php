<?php
class ControllerProductProduct extends Controller {
	private $error = array();

	public function index()
	{
		$this->load->language('product/product');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$this->load->model('catalog/category');

		if (isset($this->request->get['path'])) {
			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = $path_id;
				} else {
					$path .= '_' . $path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path)
					);
				}
			}

			// Set the last category breadcrumb
			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				if (isset($this->request->get['limit'])) {
					$url .= '&limit=' . $this->request->get['limit'];
				}

				$data['breadcrumbs'][] = array(
					'text' => $category_info['name'],
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url)
				);
			}
		}

		if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
			$url = '';

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_search'),
				'href' => $this->url->link('product/search', $url)
			);
		}

		if (isset($this->request->get['product_id'])) {
			$product_id = $this->request->get['product_id'];
		} else {
			$product_id = 0;
		}

		//$this->load->model('catalog/product');
		$this->load->model('elasticsearch/products');

		//$product_info = $this->model_catalog_product->getProduct($product_id);
		$product_info = $this->model_elasticsearch_products->getProductDetail($product_id);
		$product_des = $this->model_elasticsearch_products->getProductDescription($product_id);

		if (empty($product_des)) {
			$product_des[0]['_source']['goods_description_history'] = '';
		}

		if ($product_info) {
			$product_info['minimum'] = "1";
			$product_info['tax_class_id'] = "9";
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $product_info['goods_title'],
				'href' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id'])
			);

			$this->document->setTitle($product_info['goods_title']);
			$this->document->setDescription($product_info['goods_title']);
			$this->document->setKeywords($product_info['goods_title']);
			$this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');
			$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

			$data['heading_title'] = $product_info['goods_title'];
			$data['heading_title_cn'] = $product_info['goods_title_cn'];
			$data['text_select'] = $this->language->get('text_select');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_reward'] = $this->language->get('text_reward');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_stock'] = $this->language->get('text_stock');
			$data['text_discount'] = $this->language->get('text_discount');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_option'] = $this->language->get('text_option');
			$data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
			$data['text_write'] = $this->language->get('text_write');
			$data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', true), $this->url->link('account/register', '', true));
			$data['text_note'] = $this->language->get('text_note');
			$data['text_tags'] = $this->language->get('text_tags');
			$data['text_related'] = $this->language->get('text_related');
			//$data['text_payment_recurring'] = $this->language->get('text_payment_recurring');
			$data['text_loading'] = $this->language->get('text_loading');

			$data['entry_qty'] = $this->language->get('entry_qty');
			$data['entry_name'] = $this->language->get('entry_name');
			$data['entry_review'] = $this->language->get('entry_review');
			$data['entry_rating'] = $this->language->get('entry_rating');
			$data['entry_good'] = $this->language->get('entry_good');
			$data['entry_bad'] = $this->language->get('entry_bad');

			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_upload'] = $this->language->get('button_upload');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['button_submit_review'] = $this->language->get('button_submit_review');

			$this->load->model('catalog/review');

			$data['tab_description'] = $this->language->get('tab_description');
			$data['tab_attribute'] = $this->language->get('tab_attribute');
			$data['tab_review'] = sprintf($this->language->get('tab_review'), $product_info['goods_review_current_count']);

			$data['product_id'] = $this->request->get['product_id'];
			$data['manufacturer'] = $product_info['goods_brand'];
			//$data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
			$data['model'] = $product_info['goods_model'];
			//$data['reward'] = $product_info['reward'];
			//$data['points'] = $product_info['points'];
			$data['description'] = html_entity_decode($product_des[0]['_source']['goods_description_history'], ENT_QUOTES, 'UTF-8');
			$data['stock'] = $this->language->get('text_instock');
			if ($product_info['goods_main_image_url_local'] != '' and file_exists($product_info['goods_main_image_url_local'])==true) {
				$data['popup'] = $product_info['goods_main_image_url_local'];
				$data['thumb'] = $product_info['goods_main_image_url_local'];
			} else{
				$data['popup'] = $product_info['goods_main_image_url'];
			    $data['thumb'] = $product_info['goods_main_image_url'];
		    }
		$product_imgs = $this->model_elasticsearch_products->getProductImages($product_id);
		$data['images'] = array();

		foreach ($product_imgs as $images) {
			if ($images['_source']['goods_image_url_local'] != '' and file_exists($images['_source']['goods_image_url_local'])==true) {
				$data['images'][] = array(
					'popup' => $images['_source']['goods_image_url_local'],
					'thumb' => $images['_source']['goods_image_url_local']
				);
			} else {
				$data['images'][] = array(
					'popup' => $images['_source']['goods_image_url'],
					'thumb' => $images['_source']['goods_image_url']
				);
			}
		}
		/*			$results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);

                    foreach ($results as $result) {
                        $data['images'][] = array(
                            'popup' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height')),
                            'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'))
                        );
                    }*/

		if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
			$data['price'] = $this->currency->format($this->tax->calculate($product_info['goods_current_price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
		} else {
			$data['price'] = false;
		}

		if ((float)$product_info['goods_current_price'] < (float)$product_info['goods_mrsp_price']) {
			$data['special'] = $this->currency->format($this->tax->calculate($product_info['goods_current_price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
		} else {
			$data['special'] = false;
		}

		if ($this->config->get('config_tax')) {
			$data['tax'] = $this->currency->format((float)$product_info['goods_current_price'] ? $product_info['goods_mrsp_price'] : $product_info['goods_current_price'], $this->session->data['currency']);
		} else {
			$data['tax'] = false;
		}

		/*			$discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);

                    $data['discounts'] = array();

                    foreach ($discounts as $discount) {
                        $data['discounts'][] = array(
                            'quantity' => $discount['quantity'],
                            'price'    => $this->currency->format($this->tax->calculate($discount['goods_cad_mrsp_price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'])
                        );
                    }*/

		$data['options'] = array();
		$product_option_value = $this->model_elasticsearch_products->getProductVariation($product_info['product_uuid']);
		$product_variation_name_check = array();
		foreach($product_option_value as $var_name)	{
			if($var_name['_source']['goods_variation_name']!='' and $var_name['_source']['goods_status'] == 1){
				$product_variation_name_check[] = array(
					'goods_variation_name' => $var_name['_source']['goods_variation_name']
				);
			}
		}
		if (!empty($product_option_value) and count($product_variation_name_check)>1) {
			$option = array();
			$option['product_option_id'] = "4";
			$option['option_id'] = "5";
			$option['name'] = "Radio";
			$option['type'] = "radio";
			$option['value'] = "";
			$option['required'] = "1";
			$product_option_value_data = array();
			foreach ($product_option_value as $option_value) {
				if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['_source']['goods_current_price']) {
					$price = $this->currency->format($this->tax->calculate($option_value['_source']['goods_current_price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
				} else {
					$price = false;
				}
				$variation_name = $option_value['_source']['goods_variation_name'];
				$product_option_value_data[] = array(
					'product_option_value_id' => $this->url->link('product/product', 'product_id=' . $option_value['_source']['goods_uuid']),
					'name' => $variation_name,
					'price' => $price,
					'price_prefix' => ""
				);
			}

			$data['options'][] = array(
                'product_option_id' => $option['product_option_id'],
                'product_option_value' => $product_option_value_data,
                'option_id' => $option['option_id'],
                'name' => $option['name'],
                'type' => $option['type'],
                'value' => $option['value'],
                'required' => $option['required']
            );
		}

		if ($product_info['minimum']) {
			$data['minimum'] = $product_info['minimum'];
		} else {
			$data['minimum'] = 1;
		}

		$data['review_status'] = $this->config->get('config_review_status');

		if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
			$data['review_guest'] = true;
		} else {
			$data['review_guest'] = false;
		}

		if ($this->customer->isLogged()) {
			$data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
		} else {
			$data['customer_name'] = '';
		}

		$data['reviews'] = sprintf($this->language->get('text_reviews'), (int)$product_info['goods_review_current_count']);
		$data['rating'] = $product_info['goods_review_current_stars'];

		// Captcha
		if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
			$data['captcha'] = $this->load->controller('captcha/' . $this->config->get('config_captcha'));
		} else {
			$data['captcha'] = '';
		}

		$data['share'] = $this->url->link('product/product', 'product_id=' . $this->request->get['product_id']);

		$data['attribute_groups'] = array();

		//$data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);

		$data['products'] = array();

		/*$results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);

        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price = false;
            }

            if ((float)$result['special']) {
                $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $special = false;
            }

            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
            } else {
                $tax = false;
            }

            if ($this->config->get('config_review_status')) {
                $rating = (int)$result['rating'];
            } else {
                $rating = false;
            }

            $data['products'][] = array(
                'product_id'  => $result['product_id'],
                'thumb'       => $image,
                'name'        => $result['name'],
                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                'price'       => $price,
                'special'     => $special,
                'tax'         => $tax,
                'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
                'rating'      => $rating,
                'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
            );
        }

        $data['tags'] = array();

        if ($product_info['tag']) {
            $tags = explode(',', $product_info['tag']);

            foreach ($tags as $tag) {
                $data['tags'][] = array(
                    'tag'  => trim($tag),
                    'href' => $this->url->link('product/search', 'tag=' . trim($tag))
                );
            }
        }*/

		//$data['recurrings'] = $this->model_catalog_product->getProfiles($this->request->get['product_id']);

		//$this->model_catalog_product->updateViewed($this->request->get['product_id']);
		$data['tags'] = array();

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('product/product', $data));
	}else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/product', $url . '&product_id=' . $product_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');
                        


			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

	public function review() {
		$this->load->language('product/product');

		$this->load->model('catalog/review');

		$data['text_no_reviews'] = $this->language->get('text_no_reviews');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['reviews'] = array();

		$review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);

		$results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id'], ($page - 1) * 5, 5);

		foreach ($results as $result) {
			$data['reviews'][] = array(
				'author'     => $result['author'],
				'text'       => nl2br($result['text']),
				'rating'     => (int)$result['rating'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}

		$pagination = new Pagination();
		$pagination->total = $review_total;
		$pagination->page = $page;
		$pagination->limit = 5;
		$pagination->url = $this->url->link('product/product/review', 'product_id=' . $this->request->get['product_id'] . '&page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));

		$this->response->setOutput($this->load->view('product/review', $data));
	}

	public function write() {
		$this->load->language('product/product');

		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
				$json['error'] = $this->language->get('error_name');
			}

			if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 1000)) {
				$json['error'] = $this->language->get('error_text');
			}

			if (empty($this->request->post['rating']) || $this->request->post['rating'] < 0 || $this->request->post['rating'] > 5) {
				$json['error'] = $this->language->get('error_rating');
			}

			// Captcha
			if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
				$captcha = $this->load->controller('captcha/' . $this->config->get('config_captcha') . '/validate');

				if ($captcha) {
					$json['error'] = $captcha;
				}
			}

			if (!isset($json['error'])) {
				$this->load->model('catalog/review');

				$this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);

				$json['success'] = $this->language->get('text_success');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getRecurringDescription() {
		$this->load->language('product/product');
		$this->load->model('catalog/product');

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		if (isset($this->request->post['recurring_id'])) {
			$recurring_id = $this->request->post['recurring_id'];
		} else {
			$recurring_id = 0;
		}

		if (isset($this->request->post['quantity'])) {
			$quantity = $this->request->post['quantity'];
		} else {
			$quantity = 1;
		}

		$product_info = $this->model_catalog_product->getProduct($product_id);
		$recurring_info = $this->model_catalog_product->getProfile($product_id, $recurring_id);

		$json = array();

		if ($product_info && $recurring_info) {
			if (!$json) {
				$frequencies = array(
					'day'        => $this->language->get('text_day'),
					'week'       => $this->language->get('text_week'),
					'semi_month' => $this->language->get('text_semi_month'),
					'month'      => $this->language->get('text_month'),
					'year'       => $this->language->get('text_year'),
				);

				if ($recurring_info['trial_status'] == 1) {
					$price = $this->currency->format($this->tax->calculate($recurring_info['trial_price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$trial_text = sprintf($this->language->get('text_trial_description'), $price, $recurring_info['trial_cycle'], $frequencies[$recurring_info['trial_frequency']], $recurring_info['trial_duration']) . ' ';
				} else {
					$trial_text = '';
				}

				$price = $this->currency->format($this->tax->calculate($recurring_info['price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

				if ($recurring_info['duration']) {
					$text = $trial_text . sprintf($this->language->get('text_payment_description'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				} else {
					$text = $trial_text . sprintf($this->language->get('text_payment_cancel'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				}

				$json['success'] = $text;
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function translate_title(){
		$this->load->model('elasticsearch/products');
		$cn_title="";
		if (isset($this->request->get['prod_uuid'])){
			$prod_uuid=$this->request->get['prod_uuid'];
			$query=$this->db->query("select seller_product_title,seller_product_title_cn from freelife_mdata_goods WHERE seller_product_uuid='".$prod_uuid."'");
			if ($query->num_rows) {
				$cn_title=trim($query->row['seller_product_title_cn']);
				$en_title=trim($query->row['seller_product_title']);
				if ($cn_title==""){
					$cn_title= $this->model_elasticsearch_products->translate_reverse($en_title);
					$this->db->query("UPDATE `freelife_mdata_goods` SET seller_product_title_cn='".$cn_title."' WHERE seller_product_uuid='".$prod_uuid."'");
				}
			}
		}
		echo $cn_title;

    }
	public function v3($namespace, $name)
	{
		// Get hexadecimal components of namespace
		$nhex = str_replace(array('-','{','}'), '', $namespace);
		// Binary Value
		$nstr = '';
		// Convert Namespace UUID to bits
		for($i = 0; $i < strlen($nhex); $i+=2)
		{
			$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
		}
		// Calculate hash value
		$hash = md5($nstr . $name);
		return sprintf('%08s-%04s-%04x-%04x-%12s',
			// 32 bits for "time_low"
			substr($hash, 0, 8),
			// 16 bits for "time_mid"
			substr($hash, 8, 4),
			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 3
			(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,
			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,
			// 48 bits for "node"
			substr($hash, 20, 12)
		);
	}

	public function check_price(){
		$this->load->model('elasticsearch/products');
		if (isset($this->request->get['prod_uuid'])){
			$prod_uuid=$this->request->get['prod_uuid'];
			$query=$this->db->query("select * from freelife_mdata_price_history WHERE seller_product_uuid='".$prod_uuid."' order by seller_product_price_time desc limit 0,1");
			if ($query->num_rows) {
				$current_price=trim($query->row['seller_product_price']);
				$newest_price_update_time=trim($query->row['seller_product_price_time']);
				date_default_timezone_set("America/Vancouver");
				$date = date('Y-m-d H:i:s');
				$current_time = strtotime($date);
				$newest_price_update_time = strtotime($newest_price_update_time);
				$time_diff = $current_time - $newest_price_update_time;
				if (($time_diff) > 14400){
					$url_query=$this->db->query("select seller_product_current_url,seller_product_status from freelife_mdata_goods WHERE seller_product_uuid='".$prod_uuid."'");
					if ($url_query->num_rows) {
						$prod_url = rtrim(trim($url_query->row['seller_product_current_url']), "/");
						$prod_status = $url_query->row['seller_product_status'];
						if ($prod_status==1){
						    $new_price = $this->model_elasticsearch_products->check_price_api($prod_url);
						    $cn_price = ceil(floatval($new_price) * 7.5);
						    if ($cn_price != floatval($current_price)) {
							    if ($cn_price <= 0) {
								    $this->db->query("UPDATE `freelife_mdata_goods` SET seller_product_status = 0 where seller_product_uuid='" . $prod_uuid . "'");
                                    echo '没有库存';
								} else {
									$this->db->query("UPDATE `freelife_mdata_goods` SET seller_product_current_price='" . $cn_price . "' WHERE seller_product_uuid='" . $prod_uuid . "'");
									$current_mrsp = trim($query->row['seller_product_current_mrsp']);
									$original_price = trim($query->row['seller_product_original_price']);
									$original_mrsp = trim($query->row['seller_product_original_mrsp']);
									$currency = trim($query->row['seller_product_original_currency']);
									$currency_rate = trim($query->row['seller_product_original_currency_rate']);
									$shipping_cost = trim($query->row['seller_product_shipping_cost']);
									$customs_cost = trim($query->row['seller_product_customs_cost']);
									$price_history_status = trim($query->row['seller_product_price_history_status']);
									$name = $prod_uuid . ' '. $new_price . ' '.$date;
									$price_history_uuid = $this->v3('6ba7b810-9dad-11d1-80b4-00c04fd430c8', $name);
									$this->db->query("Insert into freelife_mdata_price_history 
												  (seller_product_price_history_uuid,seller_product_uuid,seller_product_price,
												  seller_product_current_mrsp,seller_product_price_time,seller_product_original_price,
												  seller_product_original_mrsp,seller_product_original_currency,seller_product_original_currency_rate,
												  seller_product_shipping_cost,seller_product_customs_cost,seller_product_price_history_status,
												  is_modified) 
												  Values('$price_history_uuid','$prod_uuid','$cn_price','$current_mrsp','$date',
												  '$original_price','$original_mrsp','$currency','$currency_rate','$shipping_cost','$customs_cost',
												  '$price_history_status','0')"
									);
								echo $cn_price;
							    }
						    }
						    else {
								echo $current_price;
							}
						} else {
							echo '没有库存';
						}
					}
				}
				else{
					echo $current_price;
				}
			}
		}
	}
}
