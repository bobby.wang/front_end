<?php
class bolt_ControllerCommonHeader extends ControllerCommonHeader {

public function preRender( $template_buffer, $template_name, &$data ) {
      if (!$this->endsWith( $template_name, '/template/common/header.tpl' )) {
      return parent::preRender( $template_buffer, $template_name, $data );
    }
       
        // add new controller variables


            $this->load->language( 'common/header' );
            $data['text_menu'] = $this->language->get( 'text_menu' );
            $data['text_all_categories'] = $this->language->get( 'text_all_categories' );
            $data['text_welcome'] = $this->language->get( 'text_welcome' );
            $data['text_home'] = $this->language->get('text_home');
            $data['text_menu'] = $this->language->get('text_menu');
            $data['text_register'] = $this->language->get('text_register');
            $data['text_or'] = $this->language->get('text_or');
            $data['register'] = $this->url->link('account/register', '', 'SSL');
            $data['text_blog'] = $this->language->get( 'text_blog' );
            $data['blog_href']=$this->url->link('magikblog/article');
            $data['text_information'] = $this->language->get('text_information');    

            $this->load->model('setting/setting');
            $data['cbim_data']=$this->model_setting_setting->getSetting('custom_menu_content');
            $this->load->model('tool/image');
          

          
       $data['text_welcome'] = sprintf($this->language->get('text_welcome'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'));
       $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));

        if (isset($this->request->get['category_id'])) {
        $data['category_id'] = $this->request->get['category_id'];
        } else {
        $data['category_id'] = 0;
        }

       $this->load->model('catalog/category');
       $this->load->model('catalog/product');

        // for only Top Categories
        $data['categories1'] = array();
        $categories_1 = $this->model_catalog_category->getCategories(0);
          

          foreach ($categories_1 as $category_1) {
            if($category_1['top']){
             $level_2_data = array();
             
             $categories_2 = $this->model_catalog_category->getCategories($category_1['category_id']);
             
             foreach ($categories_2 as $category_2) {
                $level_3_data = array();
                
                $categories_3 = $this->model_catalog_category->getCategories($category_2['category_id']);
                
                foreach ($categories_3 as $category_3) {
                   $level_3_data[] = array(
                      'name' => $category_3['name'],
                                           'column'   => $category_3['column'] ? $category_3['column'] : 1,
                      'href' => $this->url->link('product/search', 'path=' . $category_1['category_id'] . '_' . $category_2['category_id'] . '_' . $category_3['category_id']),
                      'category_id'=> $category_3['category_id']
                   );
                }
                
                $level_2_data[] = array(
                   'name'     => $category_2['name'],
                   'children' => $level_3_data,
                   'href'     => $this->url->link('product/search', 'path=' . $category_1['category_id'] . '_' . $category_2['category_id']),
                   'category_id'=> $category_2['category_id']   
                );               
             }
             
             $data['categories1'][] = array(
                'name'     => $category_1['name'],
                'children' => $level_2_data,
                'column'   => $category_1['column'] ? $category_1['column'] : 1,
                'href'     => $this->url->link('product/search', 'path=' . $category_1['category_id']),
                'category_id'=> $category_1['category_id']
             );
          } 
        }



     // For manufacture page specific css
      if (isset($this->request->get['route'])) {
        if (isset($this->request->get['manufacturer_id'])) {
            $brand_class=$this->request->get['route'];
            $data['brand_class'] = str_replace('/', '-', $this->request->get['route']);//exit;
        } else { $brand_class=''; $data['brand_class']='';  }
      } 

      // for information links on header
        $this->load->model('catalog/information');

        $data['informations'] = array();

        foreach ($this->model_catalog_information->getInformations() as $result) {
          if ($result['bottom']) {
            $data['informations'][] = array(
              'title' => $result['title'],
              'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
            );
          }
        }


        // call parent method
        return parent::preRender( $template_buffer, $template_name, $data );
    }
private function endsWith( $haystack, $needle ) {
    if (strlen( $haystack ) < strlen( $needle )) {
      return false;
    }
    return (substr( $haystack, strlen($haystack)-strlen($needle), strlen($needle) ) == $needle);
  }
}